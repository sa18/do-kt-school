package com.digitaloctober.ktschool.model.shipModel

import kotlinx.serialization.Serializable
import java.math.BigDecimal

typealias RollFactor = Float // 0..1

enum class MastType {
    FOREMAST,
    MAINMAST,
    MIZENMAST,
    BOUSHPRITE
}

@Serializable
data class Sail(val name: String, var rollFactor: RollFactor = 0f) {
    fun roll() {
        rollFactor = 1f
    }

    fun open() {
        rollFactor = 0f
    }
}

@Serializable
data class Mast(val type: MastType, val sails: Collection<Sail>) {
    fun rollSails() =
        sails.forEach { it.roll() }

    fun openSails() =
        sails.forEach { it.open() }

    init {
        require(type != MastType.BOUSHPRITE || sails.isEmpty()) { "Бушприт не должен содержать парусов" }
    }
}

@Serializable
data class Ship(val name: String, val masts: Collection<Mast>) {
    fun rollSails() =
        masts.forEach { it.rollSails() }

    fun openSails() =
        masts.forEach { it.openSails() }

    fun randomizeSails() =
        masts.forEach { if (Math.random() < 0.5) it.openSails() else it.rollSails() }

    val rollFactor get() = masts.flatMap { it.sails.map { sail -> sail.rollFactor } }.average()
}

@Serializable
data class Fleet(val ships: Collection<Ship>)

object FleetBuilder {
    fun build() = Fleet(
        listOf(
            //
            Ship(
                "Имперский", listOf(
                    Mast(
                        MastType.BOUSHPRITE,
                        emptyList()
                    ),
                    Mast(
                        MastType.FOREMAST, listOf(
                            Sail("royal"),
                            Sail("course")
                        )
                    ),
                    Mast(
                        MastType.MAINMAST, listOf(
                            Sail("skysail"),
                            Sail("royal"),
                            Sail("gallant"),
                            Sail("top"),
                            Sail("lower"),
                            Sail("course")
                        )
                    ),
                    Mast(
                        MastType.MIZENMAST, listOf(
                            Sail("gallant"),
                            Sail("course"),
                            Sail("spanker")
                        )
                    )
                )
            ).also {
                it.randomizeSails()
            },

            //
            Ship(
                "Торговый", listOf(
                    Mast(
                        MastType.FOREMAST, listOf(
                            Sail("royal"),
                            Sail("course")
                        )
                    ),
                    Mast(
                        MastType.MAINMAST, listOf(
                            Sail("skysail"),
                            Sail("royal"),
                            Sail("gallant"),
                            Sail("top"),
                            Sail("lower"),
                            Sail("course")
                        )
                    ),
                    Mast(
                        MastType.MIZENMAST, listOf(
                            Sail("gallant"),
                            Sail("course")
                        )
                    )
                )
            ).also {
                it.randomizeSails()
            }
        )
    )
}
