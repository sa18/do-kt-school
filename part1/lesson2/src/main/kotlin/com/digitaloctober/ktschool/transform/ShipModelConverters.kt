package com.digitaloctober.ktschool.transform

import com.digitaloctober.ktschool.model.shipModel.Fleet
import com.digitaloctober.ktschool.model.shipModel.MastType
import com.digitaloctober.ktschool.model.shipRelModel.Mast
import com.digitaloctober.ktschool.model.shipRelModel.Sail
import com.digitaloctober.ktschool.model.shipRelModel.Ship
import com.digitaloctober.ktschool.model.shipRelModel.Fleet as ShipRelModelFleet

object ShipModelConverters {
    fun shipModelToShipRelModel(fleet: Fleet): ShipRelModelFleet {
        val ships = mutableListOf<Ship>()
        val masts = mutableListOf<Mast>()
        val sails = mutableListOf<Sail>()
        var mastId = 1
        var sailId = 1
        var shipId = 1
        fleet.ships.forEach { ship ->
            ship.masts.forEach { mast ->
                mast.sails.forEach { sail ->
                    sails.add(Sail(sailId++, mastId, sail.name, sail.rollFactor))
                }
                masts.add(Mast(mastId++, shipId, mast.type.toString()))
            }
            ships.add(Ship(shipId++, ship.name))
        }
        return ShipRelModelFleet(ships, masts, sails)
    }

    fun shipRelModelToShipModel(fleet: ShipRelModelFleet): Fleet {
        val sailsByMast = fleet.sails.groupBy { it.mastId }
        val mastsByShip = fleet.masts.groupBy { it.shipId }
        val ships = fleet.ships.map { ship ->
            val masts = (mastsByShip[ship.id] ?: emptyList())
                .map { mast ->
                    val sails = (sailsByMast[mast.id] ?: emptyList())
                        .map { sail ->
                            com.digitaloctober.ktschool.model.shipModel.Sail(sail.name, sail.rollFactor)
                        }
                    com.digitaloctober.ktschool.model.shipModel.Mast(
                        MastType.valueOf(
                            mast.name.toUpperCase()
                        ), sails
                    )
                }
            com.digitaloctober.ktschool.model.shipModel.Ship(ship.name, masts)
        }
        return Fleet(ships)
    }
}
