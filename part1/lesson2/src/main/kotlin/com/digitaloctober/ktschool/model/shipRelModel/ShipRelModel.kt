package com.digitaloctober.ktschool.model.shipRelModel

import kotlinx.serialization.Serializable

typealias SailId = Int
typealias MastId = Int
typealias ShipId = Int

@Serializable
data class Sail(val id: SailId, val mastId: MastId, val name: String, var rollFactor: Float = 0f)
@Serializable
data class Mast(val id: MastId, val shipId: ShipId, val name: String)
@Serializable
data class Ship(val id: ShipId, val name: String)
@Serializable
data class Fleet(val ships: Collection<Ship>, val masts: Collection<Mast>, val sails: Collection<Sail>)

object FleetBuilder {
    fun build() = Fleet(
        ships = listOf(
            Ship(1, "Имперский"),
            Ship(2, "Торговый")
        ),

        masts = listOf(
            //
            Mast(1, 1, "boushprite"),
            Mast(2, 1, "foremast"),
            Mast(3, 1, "mainmast"),
            Mast(4, 1, "mizenmast"),
            //
            Mast(5, 2, "foremast"),
            Mast(6, 2, "mainmast"),
            Mast(7, 2, "mizenmast")
        ),

        sails = listOf(
            Sail(1, 2, "sky"),
            Sail(2, 2, "royal"),
            Sail(3, 2, "gallant"),
            Sail(4, 2, "course"),
            //
            Sail(5, 3, "gallant"),
            Sail(6, 3, "course"),
            //
            Sail(7, 4, "gallant"),
            Sail(8, 4, "course"),
            //
            Sail(9, 5, "gallant"),
            Sail(10, 5, "course"),
            //
            Sail(11, 6, "gallant"),
            Sail(12, 6, "course"),
            //
            Sail(13, 7, "gallant"),
            Sail(14, 7, "course")
        )
    )
}
