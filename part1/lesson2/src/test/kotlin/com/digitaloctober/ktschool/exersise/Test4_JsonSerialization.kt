package com.digitaloctober.ktschool.exersise

import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlinx.serialization.stringify
import mu.KotlinLogging
import org.junit.jupiter.api.Test
import com.digitaloctober.ktschool.model.shipModel.Fleet
import com.digitaloctober.ktschool.model.shipModel.FleetBuilder
import com.digitaloctober.ktschool.transform.ShipModelConverters
import kotlinx.serialization.Serializable

class Test4_JsonSerialization {

    private val log = KotlinLogging.logger {}
    private val json = Json(JsonConfiguration.Stable.copy(unquoted = false))

    @UseExperimental(ImplicitReflectionSerializer::class)
    @Test
    fun testJsonSerialization() {

        val fleet = FleetBuilder.build()
        log.debug { json.stringify(fleet) }

        val relationalFleet = ShipModelConverters.shipModelToShipRelModel(fleet)
        log.debug { json.stringify(relationalFleet) }
    }

    @Test
    fun testJsonParse() {
        val fleet = json.parse(Fleet.serializer(), javaClass.getResource("/data/fleet.json").readText())
        log.debug { fleet }
    }

    @Serializable
    data class AB(val a: Int, val b: Int)

    @Test
    fun testStrictModeOff() {
        val json = Json(JsonConfiguration.Stable.copy(strictMode = false, unquoted = true))
        val ab = json.parse(AB.serializer(), "{a:1, b:2, c:3}")
        log.debug { ab }
    }
}
