package com.digitaloctober.ktschool.exersise

import com.digitaloctober.ktschool.model.shipModel.FleetBuilder
import mu.KotlinLogging
import org.junit.jupiter.api.Test

class Test2 {

    private val log = KotlinLogging.logger {}

    @Test
    fun test() {

        val fleet = FleetBuilder.build()

        log.debug { fleet }

        fleet.ships.forEach {
            log.debug(" корабль ${it.name} rollFactor = ${it.rollFactor}")
        }

    }
}
