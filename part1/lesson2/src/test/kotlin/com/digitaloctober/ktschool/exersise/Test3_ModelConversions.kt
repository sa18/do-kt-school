package com.digitaloctober.ktschool.exersise

import assertk.assertThat
import assertk.assertions.isEqualTo
import mu.KotlinLogging
import org.junit.jupiter.api.Test
import com.digitaloctober.ktschool.model.shipModel.FleetBuilder
import com.digitaloctober.ktschool.transform.ShipModelConverters

class Test3_ModelConversions {

    private val log = KotlinLogging.logger {}

    @Test
    fun test() {

        val fleet = FleetBuilder.build()
        val fleetRelational = ShipModelConverters.shipModelToShipRelModel(fleet)
        val fleet2 = ShipModelConverters.shipRelModelToShipModel(fleetRelational)

        assertThat(fleet.toString()).isEqualTo(fleet2.toString())

        log.debug { fleetRelational }
    }
}
