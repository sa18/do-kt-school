package com.digitaloctober.ktschool.exersise

import com.digitaloctober.ktschool.repository._RepositoryConfig
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Import
import org.springframework.transaction.annotation.EnableTransactionManagement

@EnableAutoConfiguration
@Import(_RepositoryConfig::class)
class _TestsConfig