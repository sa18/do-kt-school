package com.digitaloctober.ktschool.exersise

import com.digitaloctober.ktschool.repository.ShipRepository
import mu.KotlinLogging
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration

@SpringBootTest
@ContextConfiguration(classes = [_TestsConfig::class])
class Test2_FindShips {

    val log = KotlinLogging.logger { }

    @Autowired
    lateinit var shipRepository: ShipRepository

    @Test
    fun `ищем корабли со свёрнутыми парусами`() {

        shipRepository.findAllWithSailRollFactorGreaterThan(0.6f).also {
            log.debug { it }
        }
    }
}
