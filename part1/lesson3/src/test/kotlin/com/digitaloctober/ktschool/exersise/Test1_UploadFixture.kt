package com.digitaloctober.ktschool.exersise

import com.digitaloctober.ktschool.model.Mast
import com.digitaloctober.ktschool.model.MastType
import com.digitaloctober.ktschool.model.Sail
import com.digitaloctober.ktschool.model.Ship
import com.digitaloctober.ktschool.repository.SailRepository
import com.digitaloctober.ktschool.repository.ShipRepository
import mu.KotlinLogging
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ContextConfiguration
import javax.annotation.PostConstruct
import javax.persistence.EntityManager
import javax.transaction.Transactional

@SpringBootTest
@ContextConfiguration(classes = [_TestsConfig::class])
class Test1_UploadFixture {

    @Autowired
    lateinit var em: EntityManager

    @Autowired
    lateinit var shipRepository: ShipRepository

    @Autowired
    lateinit var sailRepository: SailRepository

    @PostConstruct
    protected fun init() {
        shipRepository.deleteAll()
    }

    @Test
    @Transactional
    @Rollback(false)
    fun `загружаем фикстуру`() {

        val ships = Fixture.build()

        shipRepository.saveAll(ships)

        log.debug { "complete" }
    }

    companion object {
        val log = KotlinLogging.logger { }
    }
}

private object Fixture {
    fun build(): List<Ship> {
        val ships = listOf(
            Ship(
                name = "Имперский",
                masts = listOf(
                    Mast(
                        type = MastType.BOUSHPRITE
                    ),
                    Mast(
                        type = MastType.FOREMAST,
                        sails = listOf(
                            Sail(name = "royal"),
                            Sail(name = "course")
                        )
                    ),
                    Mast(
                        type = MastType.MAINMAST,
                        sails = listOf(
                            Sail(name = "skysail"),
                            Sail(name = "royal"),
                            Sail(name = "gallant"),
                            Sail(name = "top"),
                            Sail(name = "lower"),
                            Sail(name = "course")
                        )
                    ),
                    Mast(
                        type = MastType.MIZENMAST,
                        sails = listOf(
                            Sail(name = "gallant"),
                            Sail(name = "course"),
                            Sail(name = "spanker")
                        )
                    )
                )
            ).also {
                it.randomizeSails()
            },

            //
            Ship(
                name = "Торговый",
                masts = listOf(
                    Mast(
                        type = MastType.FOREMAST,
                        sails = listOf(
                            Sail(name = "royal"),
                            Sail(name = "course")
                        )
                    ),
                    Mast(
                        type = MastType.MAINMAST,
                        sails = listOf(
                            Sail(name = "skysail"),
                            Sail(name = "royal"),
                            Sail(name = "gallant"),
                            Sail(name = "top"),
                            Sail(name = "lower"),
                            Sail(name = "course")
                        )
                    ),
                    Mast(
                        type = MastType.MIZENMAST,
                        sails = listOf(
                            Sail(name = "gallant"),
                            Sail(name = "course")
                        )
                    )
                )
            ).also {
                it.randomizeSails()
            })

        // rebind objects
        ships.forEach { ship ->
            ship.masts.forEach { mast ->
                mast.ship = ship
                mast.sails.forEach { sail ->
                    sail.mast = mast
                }
            }
        }

        return ships
    }
}
