package com.digitaloctober.ktschool.repository

import com.digitaloctober.ktschool.model.Ship
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@ComponentScan
@EnableTransactionManagement
@EnableJpaRepositories
@EntityScan(basePackageClasses = [Ship::class])
@EnableConfigurationProperties(DataSourceProperties::class)
class _RepositoryConfig