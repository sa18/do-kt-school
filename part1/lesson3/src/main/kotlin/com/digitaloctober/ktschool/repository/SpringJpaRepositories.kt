package com.digitaloctober.ktschool.repository

import com.digitaloctober.ktschool.model.Mast
import com.digitaloctober.ktschool.model.RollFactor
import com.digitaloctober.ktschool.model.Sail
import com.digitaloctober.ktschool.model.Ship
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ShipRepository : JpaRepository<Ship, Long> {
    @Query("select distinct s from Ship s left join s.masts m left join m.sails sails where sails.rollFactor > ?1")
    fun findAllWithSailRollFactorGreaterThan(rollFactor: RollFactor) : Collection<Ship>
}

@Repository
interface MastRepository : JpaRepository<Mast, Long>

@Repository
interface SailRepository : JpaRepository<Sail, Long> {
    fun findByRollFactorGreaterThan(rollFactor: RollFactor)
}
