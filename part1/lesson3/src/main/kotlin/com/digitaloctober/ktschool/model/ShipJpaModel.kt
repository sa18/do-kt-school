package com.digitaloctober.ktschool.model

import javax.persistence.*

typealias RollFactor = Float // 0..1

enum class MastType {
    FOREMAST,
    MAINMAST,
    MIZENMAST,
    BOUSHPRITE
}

@Entity
data class Sail(
    @Id @GeneratedValue val id: Long? = null,
    val name: String
) {

    @ManyToOne(cascade = [CascadeType.PERSIST], optional = false)
    lateinit var mast: Mast

    var rollFactor: RollFactor = 0f

    fun roll() {
        rollFactor = 1f
    }

    fun open() {
        rollFactor = 0f
    }
}

@Entity
data class Mast(
    @Id @GeneratedValue val id: Long? = null,
    val type: MastType,
    @OneToMany(mappedBy = "mast", fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    val sails: Collection<Sail> = mutableSetOf()
) {

    @ManyToOne(cascade = [CascadeType.PERSIST], optional = false)
    lateinit var ship: Ship

    fun rollSails() =
        sails.forEach { it.roll() }

    fun openSails() =
        sails.forEach { it.open() }

    init {
        require(type != MastType.BOUSHPRITE || sails.isEmpty()) { "Бушприт не должен содержать парусов" }
    }
}

@Entity
data class Ship(
    @Id @GeneratedValue val id: Long? = null,
    val name: String,
    @OneToMany(mappedBy = "ship", fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    val masts: Collection<Mast> = mutableSetOf()
) {

    fun rollSails() =
        masts.forEach { it.rollSails() }

    fun openSails() =
        masts.forEach { it.openSails() }

    fun randomizeSails() =
        masts.forEach { if (Math.random() < 0.5) it.openSails() else it.rollSails() }

    val rollFactor get() = masts.flatMap { it.sails.map { sail -> sail.rollFactor } }.average()
}
