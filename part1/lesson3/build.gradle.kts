import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    id("org.jetbrains.kotlin.plugin.jpa").version("1.3.50") // генерирует всем @Entity пустой конструктор
    id("org.jetbrains.kotlin.plugin.spring").version("1.3.50") // делает компоненты Spring открытыми
}

repositories {
    mavenCentral()
}

dependencies {
    // Spring
    implementation("org.springframework.boot:spring-boot-starter:2.1.8.RELEASE")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:2.1.8.RELEASE")
    implementation("org.springframework.boot:spring-boot-starter-web:2.1.8.RELEASE")
    // Kotlin
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    // Logging
    implementation("io.github.microutils:kotlin-logging:1.7+")
    implementation("ch.qos.logback:logback-classic:1.2.+")
    // Jackson
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.10")
    // JPA
    implementation("org.hibernate:hibernate-core:5.+")
    // H2
    implementation("com.h2database:h2:1.4.199")
    // JUnit
    testImplementation("org.springframework.boot:spring-boot-starter-test:2.1.8.RELEASE") {
        exclude(module = "junit")
    }
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.+")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.+")
    testImplementation("com.willowtreeapps.assertk:assertk-jvm:0.20")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
        suppressWarnings = true
        freeCompilerArgs = listOf("-Xjsr305=strict")
    }
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform { }
}

allOpen {
    annotation("javax.persistence.Entity")
    annotation("javax.persistence.MappedSuperclass")
    annotation("javax.persistence.Embeddable")
    // + компоненты Spring добавляются автоматически через plugin kotlin-spring
}
