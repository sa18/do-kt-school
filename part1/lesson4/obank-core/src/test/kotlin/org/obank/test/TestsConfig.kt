package org.obank.test

import org.obank.config.CoreConfig
import org.obank.config.DatabaseConfig
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@EnableAutoConfiguration
@Import(CoreConfig::class, DatabaseConfig::class)
class TestsConfig
