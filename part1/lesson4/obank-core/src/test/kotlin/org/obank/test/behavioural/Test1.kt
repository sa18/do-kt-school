package org.obank.test.behavioural

import mu.KotlinLogging
import org.jooq.DSLContext
import org.junit.jupiter.api.Test
import org.obank.db.schema_public.Tables
import org.obank.service.BankSimulatorService
import org.obank.test.TestsConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.transaction.annotation.Transactional

@SpringBootTest
@ContextConfiguration(classes = [TestsConfig::class])
@Transactional
class Test1 {

    @Autowired
    private lateinit var db: DSLContext

    @Autowired
    private lateinit var bankSimulatorService: BankSimulatorService

    @Test
    fun test() {

        bankSimulatorService.issueBankingCards(10)

        db.selectFrom(Tables.CARD).fetch().forEach {
            log.debug { it }
        }
    }

    private val log = KotlinLogging.logger { }
}
