package org.obank.test.unit.service

import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.slot
import mu.KotlinLogging
import org.jooq.DSLContext
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.obank.db.schema_public.tables.records.SmsLogRecord
import org.obank.repository.CustomerRepo
import org.obank.service.BankSimulatorService
import org.obank.service.CardService
import org.obank.service.CustomerAuthService
import org.obank.service.SmsGatewayOnlineService
import org.obank.test.TestsConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.util.ReflectionTestUtils
import org.springframework.transaction.annotation.Transactional
import javax.annotation.PostConstruct

@SpringBootTest
@ContextConfiguration(classes = [TestsConfig::class])
@Transactional
class CustomerAuthService_Test {

    private val log = KotlinLogging.logger { }

    @MockK(relaxUnitFun = true)
    private lateinit var fakeSmsGateway: SmsGatewayOnlineService

    @Autowired
    private lateinit var customerAuthService: CustomerAuthService

    @Autowired
    private lateinit var customerRepo: CustomerRepo

    @Autowired
    private lateinit var cardService: CardService

    @Autowired
    private lateinit var db: DSLContext

    @Autowired
    private lateinit var bankSimulatorService: BankSimulatorService

    @PostConstruct
    protected fun init() {
        MockKAnnotations.init(this)
        ReflectionTestUtils.setField(customerAuthService, "smsGatewayOnlineService", fakeSmsGateway)
    }

    @Test
    fun testRegisterAndAuthVerify() {

        val smsCaptor = slot<SmsLogRecord>()
        every {
            fakeSmsGateway.sendSms(capture(smsCaptor))
        } answers {
            log.debug("fake sms sent : ${smsCaptor.captured.content}")
        }

        val phone = "+15556677"
        customerRepo.findValidByMobilePhone(phone)?.let { existingCustomer ->
            customerRepo.updateIsValid(existingCustomer.id, false)
        }

        val card = bankSimulatorService.issueBankingCards(1).first()

        customerAuthService.registerNew(card.paymentNumber, "Дональд Трамп", phone, "donald@wh.gov")
        val token = customerAuthService.authBegin(phone)
        customerAuthService.authVerify(token, smsCaptor.captured.authCode)
        Assertions.assertTrue(true)
    }

}
