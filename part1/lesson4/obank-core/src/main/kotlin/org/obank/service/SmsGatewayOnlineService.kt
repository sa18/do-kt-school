package org.obank.service

import mu.KotlinLogging
import org.jsoup.Jsoup
import org.obank.db.schema_public.tables.records.SmsLogRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Service
import java.net.URLEncoder
import java.nio.charset.StandardCharsets.UTF_8

@ConfigurationProperties(prefix = "service.sms-gateway")
class SmsGatewayOnlineServiceProperties {
    var enable: Boolean = true
    var login: String = ""
    var password: String = ""
}

@Service
@EnableConfigurationProperties(SmsGatewayOnlineServiceProperties::class)
class SmsGatewayOnlineService {

    private val log = KotlinLogging.logger { }

    @Autowired
    private lateinit var props: SmsGatewayOnlineServiceProperties

    fun sendSms(sms: SmsLogRecord) {

        if (!props.enable) {
            log.debug("Работа шлюза запрещена в настройках, запрос на отправку SMS игнорируется")
            return
        }

        val login = URLEncoder.encode(props.login, UTF_8.toString())
        val psw = URLEncoder.encode(props.password, UTF_8.toString())
        val phones = URLEncoder.encode(sms.destination, UTF_8.toString())
        val content = URLEncoder.encode(sms.content, UTF_8.toString())
        val url = "https://smsc.ru/sys/send.php?login=${login}&psw=${psw}&phones=${phones}&mes=${content}"
        log.trace("URL для отправки sms: $url")
        Jsoup.connect(url).get()
    }

}
