package org.obank.model

import java.time.LocalDate
import java.time.LocalDateTime

typealias Money = Float

enum class DebitCredit {
    DEBIT,
    CREDIT
}

enum class TransactionStatus {
    ENTERING,
    PENDING,
    COMPLETE,
    CANCELED
}

data class BankingCard(
    val cardNumber: String,
    val accountNumber: String,
    val dateValidTill: LocalDate,
    val ownerCustomerId: String?
)

data class TransactionHistoryItem(
    val id: String,
    val accountNumber: String,
    val dt: LocalDateTime,
    val debitCredit: DebitCredit,
    val status: TransactionStatus,
    val amount: Money,
    val currency: String,
    val shortDescription: String
)

data class TransactionDetails(
    val id: String,
    val accountNumber: String,
    val dt: LocalDateTime,
    val debitCredit: DebitCredit,
    val status: TransactionStatus,
    val amount: Float,
    val currency: String,
    val otherSideAccountNumber: String,
    val otherSideName: String,
    val shortDescription: String,
    val fullDescription: String,
    val canBeCanceled: Boolean
)

data class ChatMessage(
    val dt: LocalDateTime,
    val serverSideMessageId: String,
    val clientSideMessageId: String,
    val authorId: String,
    val text: String,
    val isOtherSideDelivered: Boolean,
    val isOtherSideShown: Boolean
)

data class PromoItem(
    val dt: LocalDateTime,
    val isNew: Boolean,
    val isPersonal: Boolean,
    val category: String,
    val title: String
)

data class CustomerProfile(
    val id: String,
    val valid: Boolean,
    val fio: String,
    val dtCreated: LocalDateTime,
    val email: String,
    val mobilePhone: String
)
