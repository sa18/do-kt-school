package org.obank.service

import mu.KotlinLogging
import org.obank.Util
import org.obank.db.schema_public.tables.records.CardRecord
import org.obank.db.schema_public.tables.records.ChatEventRecord
import org.obank.repository.CardRepo
import org.obank.repository.ChatEventRepo
import org.obank.repository.CustomerRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDate

@Service
@Transactional
class BankSimulatorService {

    private val log = KotlinLogging.logger { }

    @Autowired
    private lateinit var cardRepo: CardRepo

    @Autowired
    private lateinit var chatEventRepo: ChatEventRepo

    @Autowired
    private lateinit var customerRepo: CustomerRepo

    // Сгенерировать серию из N новых банковских карт
    fun issueBankingCards(n: Int): List<CardRecord> {
        log.debug("Генерирую $n банковских карточек")
        val cardsList = (0 until n).map {
            CardRecord().apply {
                dateExpire = LocalDate.now().plusDays(100)
                paymentNumber = Util.generateRandomDigits(20)
                accountNumber = paymentNumber
            }
        }

        cardsList.forEach { cardRepo.insertNew(it) }
        log.info("Выпущены $n банковских карточек")
        return cardsList
    }

    // Сгенерировать ответы на новые сообщения чата
    fun answerAllChatMessages() {

        val allMessages = chatEventRepo.findByDtCreatedWithinLastDays_dtOtherSideShownIsNull_clientIdIsNotNull(5)
        val messagesByCustomer = allMessages.groupBy { it.customerId }
        if (messagesByCustomer.isEmpty()) {
            log.trace("В чате нет новых сообщений")
            return
        }

        log.debug("Начинаю обработку сообщений из чата, кол-во ${messagesByCustomer.size}")

        var numErrors = 0
        messagesByCustomer.entries.forEach { (customerId, messages) ->

            try {
                val customer = customerRepo.getById(customerId)
                log.info("Есть входящие сообщения от клиента ${customer.id}")

                messages.forEach { message ->
                    chatEventRepo.setDtOtherSideShownToNow(message.id)
                }

                val answer = ChatEventRecord().also {
                    it.customerId = customerId
                    it.type = "A"
                    it.text = Math.random().let { v ->
                        when {
                            v < 0.3 -> "Здравствуйте, уважаемый ${customer.fio}!"
                            v < 0.5 -> "Задайте здесь свой вопрос."
                            v < 0.8 -> "Конечно, мы абсолютно уверены в этом!"
                            v < 0.9 -> "Хотите много денег?"
                            else -> "Не поняла Ваш вопрос, пожалуйста, сформулируйте иначе."
                        }
                    }
                }

                chatEventRepo.insertNew(answer)
            } catch (x: Exception) {
                log.error("Ошибка обработки сообщений из чата от клиента $customerId: ${x.localizedMessage}")
                log.debug(null, x)
                ++numErrors;
            }
        }

        if (numErrors > 0) {
            log.warn("Сообщения из чата обработаны с ошибками ($numErrors)")
        } else {
            log.info("Сообщения из чата обработаны")
        }
    }

}
