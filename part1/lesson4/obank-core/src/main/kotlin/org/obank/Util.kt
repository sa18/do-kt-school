package org.obank

import kotlin.random.Random

object Util {

    fun generateRandomDigits(length: Int) =
        (0 until length).map { Random.nextInt(10).toString() }.joinToString("")

    fun generateRandomToken(length: Int) =
        (0 until length).map { letters[Random.nextInt(letters.length)] }.joinToString("")

    private val letters = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890!@#$%^&*()_+-={}|[]"

    // based on https://stackoverflow.com/questions/16273318/transliteration-from-cyrillic-to-latin-icu4j-java
    private val cyr = charArrayOf('а','б','в','г','д','е','ё', 'ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х', 'ц','ч', 'ш','щ','ъ','ы','ь','э', 'ю','я','А','Б','В','Г','Д','Е','Ё', 'Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х', 'Ц', 'Ч','Ш', 'Щ','Ъ','Ы','Ь','Э','Ю','Я','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z')
    private val lat = arrayOf("a","b","v","g","d","e","e","zh","z","i","y","k","l","m","n","o","p","r","s","t","u","f","h","ts","ch","sh","sch", "","i", "","e","ju","ja","A","B","V","G","D","E","E","Zh","Z","I","Y","K","L","M","N","O","P","R","S","T","U","F","H","Ts","Ch","Sh","Sch", "","I", "","E","Ju","Ja","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z")
    private val cyr2lat: Map<Char, String> = cyr.mapIndexed { index, c -> c to lat[index] }.toMap()
    fun transliterate(message: String): String {
        val builder = StringBuilder()
        for (element in message) {
            builder.append(cyr2lat[element] ?: element)
        }
        return builder.toString()
    }

}
