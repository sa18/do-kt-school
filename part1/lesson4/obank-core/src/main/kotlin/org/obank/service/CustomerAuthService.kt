package org.obank.service

import mu.KotlinLogging
import org.obank.Util
import org.obank.model.*
import org.obank.repository.CardRepo
import org.obank.repository.CustomerRepo
import org.obank.repository.SmsLogRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class CustomerAuthService {

    private val log = KotlinLogging.logger { }

    @Autowired
    private lateinit var customerRepo: CustomerRepo

    @Autowired
    private lateinit var cardRepo: CardRepo

    @Autowired
    private lateinit var smsLogRepo: SmsLogRepo

    @Autowired
    private lateinit var smsGatewayOnlineService: SmsGatewayOnlineService

    @Autowired
    private lateinit var customerProfileService: CustomerProfileService

    // Зарегистрировать нового пользователя и привязать к нему банковскую карту
    fun registerNew(cardNumber: String, FIO: String, mobilePhone: String, email: String) {

        log.debug("Начало регистрации нового клиента $FIO $mobilePhone $email по карте $cardNumber")

        if (customerRepo.findValidByMobilePhone(mobilePhone) != null) {
            throw InvalidAccessException("такой пользователь уже есть")
        }

        val card = cardRepo.findByPaymentNumberForUpdate(cardNumber)
        when {
            card == null -> throw ResourceNotFoundException("такой карты нету")
            card.isExpired -> throw InvalidAccessException("карта просрочена")
            card.ownerId != null -> throw InvalidAccessException("карта уже привязана к пользователю")
        }
        card!!

        val customer = customerProfileService.createNew(FIO, mobilePhone, email)
        cardRepo.updateOwner(card.id, customer.id)
        log.info("Карта $cardNumber привязана к клиенту ${customer.id}")
    }

    // Отправить пользователю SMS с кодом авторизации
    fun authBegin(mobilePhone: String): String {

        log.debug("Начало аутентификации пользователя с номером телефона $mobilePhone")

        val cust = customerRepo.findValidByMobilePhone(mobilePhone)
        when {
            cust == null -> throw ResourceNotFoundException("пользователя не существует")
            !cust.isValid -> throw InvalidAccessException("неверный пользователь")
        }
        cust!!

        smsLogRepo.findLastNonValidatedNonExpiredByCustomer(cust.id)?.let { lastSms ->
            throw InvalidAccessException("отправка следующей SMS возможна только после ${lastSms.dtExpire}")
        }

        val authCode = Util.generateRandomDigits(4)
        val authToken = Util.generateRandomToken(16)

        log.debug("Пара для аутентификации через sms: token=$authToken code=$authCode")
        val sms = SmsLogRecordBuilder.buildForAuthCode(cust.id, mobilePhone, authCode, authToken)
        smsGatewayOnlineService.sendSms(sms)
        smsLogRepo.insertNew(sms)

        log.info("Старт аутентификации пользователя с номером телефона $mobilePhone")

        return authToken
    }

    // Верифицировать пользователя по коду, высланному в SMS
    fun authVerify(token: String, code: String): String {

        log.debug("Начало проверки аутентичности token=$token code=$code")

        val sms = smsLogRepo.findLastNonValidatedNonExpiredByAuthTokenForUpdate(token)
        when {
            sms == null -> throw AuthenticationException("ошибка аутентификации (код просрочен или не существует)")
            sms.authCode != code -> throw AuthenticationException("ошибка аутентификации (неверный код)")
        }
        sms!!

        smsLogRepo.updateDtValidated(sms.id)
        val customer = customerRepo.getById(sms.customerId)
        log.debug("Аутентичность пользователя ${customer.id} подтверждена")
        return customer.id.toString()
    }

}
