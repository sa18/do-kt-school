package org.obank.service

import mu.KotlinLogging
import org.obank.db.schema_public.tables.records.CustomerRecord
import org.obank.model.CustomerProfile
import org.obank.model.generatePic
import org.obank.model.toCustomerProfile
import org.obank.repository.CustomerRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
@Transactional
class CustomerProfileService {

    private val log = KotlinLogging.logger {  }

    @Autowired
    private lateinit var customerRepo: CustomerRepo

    // получить профиль пользователя
    fun getProfile(customerId: String): CustomerProfile =
        customerRepo.getById(UUID.fromString(customerId)).toCustomerProfile()

    // получить картинку пользователя
    fun getUserpic(customerId: String): ByteArray =
        customerRepo.getById(UUID.fromString(customerId)).pic

    // изменить данные пользователя
    fun edit(customerId: String, fio: String? = null, email: String? = null, userPic: ByteArray? = null) {
        log.debug("Начинаю обновление профиля клиента $customerId")
        val customer = customerRepo.getById(UUID.fromString(customerId))
        if (fio != null && fio.isNotBlank()) {
            log.debug("Изменение ФИО $fio")
            customer.fio = fio
        }
        if (email != null && email.isNullOrEmpty()) {
            log.debug("Изменение email $email")
            customer.email = email
        }
        if (userPic != null) {
            log.debug("Изменение картинки, новый размер ${userPic.size}")
            customer.setPic(*userPic)
        }
        customer.update()
        log.info("Профиль клиента $customerId обновлён")
    }

    fun createNew(fio: String, mobilePhone: String, email: String): CustomerRecord {
        log.debug("Создаю новый профиль клиента $fio $mobilePhone $email")
        val customer = CustomerRecord().also {
            it.fio = fio.trim()
            it.mobilePhone = mobilePhone.trim()
            it.email = email.trim()
            it.isValid = true
            it.generatePic()
        }
        customerRepo.insertNew(customer)
        log.info("Создан новый профиль клиента ${customer.id}")
        return customer
    }

    fun invalidateCustomer(customerId: String) {
        customerRepo.updateIsValid(UUID.fromString(customerId), false)
        log.info("Пользователь $customerId сделан неактивным")
    }

}
