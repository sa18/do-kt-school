package org.obank.service

import org.obank.model.PromoItem
import org.obank.repository.PromoEventRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
@Transactional
class PromoEventService {

    @Autowired
    private lateinit var promoEventRepo: PromoEventRepo

    // Получить список активных промо-событий (как персональных, так и общих)
    fun findActualPromoEvents(customerId: String): List<PromoItem> {
        val nonPersonalEvents = promoEventRepo.findActualWithoutCustomer()
        val personalEvents = promoEventRepo.findActualByCustomer(UUID.fromString(customerId))
        return (nonPersonalEvents + personalEvents).map {
            PromoItem(
                dt = it.dtCreated,
                isNew = it.isNew,
                isPersonal = it.isPersonal,
                category = it.category,
                title = it.title
            )
        }
    }

}
