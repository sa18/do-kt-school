package org.obank.config

import org.obank.repository.CardRepo
import org.obank.service.CardTransactionService
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
//@EnableConfigurationProperties(CoreProperties::class)
@ComponentScan(basePackageClasses = [CardTransactionService::class, CardRepo::class])
class CoreConfig

