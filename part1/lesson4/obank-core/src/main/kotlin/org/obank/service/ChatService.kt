package org.obank.service

import org.obank.db.schema_public.tables.records.ChatEventRecord
import org.obank.model.ChatMessage
import org.obank.repository.ChatEventRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

@Service
@Transactional
class ChatService {

    @Autowired
    private lateinit var chatEventRepo: ChatEventRepo

    // Получить историю сообщений с банком, начиная с даты
    fun getChatHistory(customerId: String, dateFrom: LocalDate): List<ChatMessage> {
        return chatEventRepo
            .findByCustomerAndDtCreatedFrom(UUID.fromString(customerId), dateFrom.atStartOfDay())
            .map {
                ChatMessage(
                    dt = it.dtCreated,
                    authorId = if (it.clientId == null) "system" else it.customerId.toString(),
                    clientSideMessageId = it.clientId,
                    isOtherSideDelivered = it.dtOtherSideDelivered != null,
                    isOtherSideShown = it.dtOtherSideShown != null,
                    serverSideMessageId = it.id.toString(),
                    text = it.text
                )
            }
    }

    // Отправить новое сообщение банку
    fun sendNewMessageFromCustomer(customerId: String, clientSideMessageId: String, text: String): String {
        val record = ChatEventRecord().also {
            it.dtCreated = LocalDateTime.now()
            it.customerId = UUID.fromString(customerId)
            it.clientId = clientSideMessageId
            it.type = "C"
            it.dtOtherSideDelivered = LocalDateTime.now()
            it.dtOtherSideShown = null
            it.text = text.trim()
        }
        chatEventRepo.insertNew(record)
        return record.id.toString()
    }

    // Отправить новое сообщение из банка
    fun sendNewMessageToCustomer(customerId: String, text: String): String {
        val record = ChatEventRecord().also {
            it.dtCreated = LocalDateTime.now()
            it.customerId = UUID.fromString(customerId)
            it.clientId = null
            it.type = "A"
            it.dtOtherSideDelivered = null
            it.dtOtherSideShown = null
            it.text = text.trim()
        }
        chatEventRepo.insertNew(record)
        return record.id.toString()
    }

}
