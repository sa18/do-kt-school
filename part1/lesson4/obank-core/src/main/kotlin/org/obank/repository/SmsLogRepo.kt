package org.obank.repository

import org.jooq.DSLContext
import org.jooq.impl.DSL
import org.obank.db.schema_public.Tables.SMS_LOG
import org.obank.db.schema_public.tables.records.SmsLogRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import java.time.LocalDateTime
import java.util.*

@Repository
class SmsLogRepo {

    @Autowired
    private lateinit var db: DSLContext

    fun insertNew(sms: SmsLogRecord) {
        sms.id = UUID.randomUUID()
        db.executeInsert(sms)
    }

    fun findLastNonValidatedNonExpiredByCustomer(customerId: UUID): SmsLogRecord? =
        db.selectFrom(SMS_LOG)
            .where(
                SMS_LOG.CUSTOMER_ID.eq(customerId)
                    .and(SMS_LOG.DT_VALIDATED.isNull)
                    .and(SMS_LOG.DT_EXPIRE.greaterThan(LocalDateTime.now()))
            )
            .orderBy(SMS_LOG.DT_SENT.desc())
            .limit(1)
            .fetchAny()

    fun findLastNonValidatedNonExpiredByAuthTokenForUpdate(token: String): SmsLogRecord? =
        db.selectFrom(SMS_LOG)
            .where(
                SMS_LOG.AUTH_TOKEN.eq(token)
                    .and(SMS_LOG.DT_VALIDATED.isNull)
                    .and(SMS_LOG.DT_EXPIRE.greaterThan(LocalDateTime.now()))
            )
            .orderBy(SMS_LOG.DT_SENT.desc())
            .limit(1)
            .forUpdate()
            .fetchAny()

    fun findLastByAuthToken(authToken: String): SmsLogRecord? =
        db.selectFrom(SMS_LOG)
            .where(SMS_LOG.AUTH_TOKEN.eq(authToken))
            .orderBy(SMS_LOG.DT_SENT.desc())
            .fetchAny()

    fun updateDtValidated(id: UUID) =
        db.update(SMS_LOG)
            .set(SMS_LOG.DT_VALIDATED, DSL.currentLocalDateTime())
            .where(SMS_LOG.ID.eq(id))
            .execute()
}
