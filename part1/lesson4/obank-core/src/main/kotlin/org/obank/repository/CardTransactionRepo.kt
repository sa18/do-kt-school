package org.obank.repository

import org.jooq.DSLContext
import org.obank.db.schema_public.Tables.CARD_TRANSACTION
import org.obank.db.schema_public.tables.records.CardTransactionRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import java.time.LocalDateTime
import java.util.*

@Repository
class CardTransactionRepo {
    @Autowired
    private lateinit var db: DSLContext

    fun insertNew(c: CardTransactionRecord) {
        c.id = UUID.randomUUID()
        db.executeInsert(c)
    }

    fun findByCardAndDtRange(
        cardId: UUID,
        dtFrom: LocalDateTime,
        dtTill: LocalDateTime
    ): Collection<CardTransactionRecord> =
        db.selectFrom(T)
            .where(T.CARD_ID.eq(cardId))
            .and(T.DT_CREATED.between(dtFrom, dtTill))
            .orderBy(T.DT_CREATED.desc())
            .fetchInto(CardTransactionRecord::class.java)

    fun getById(transactionId: UUID): CardTransactionRecord =
        db.selectFrom(T)
            .where(T.ID.eq(transactionId))
            .fetchSingle()

    fun getByIdForUpdate(transactionId: UUID): CardTransactionRecord =
        db.selectFrom(T)
            .where(T.ID.eq(transactionId))
            .forUpdate()
            .fetchSingle()

    fun findAll(): Collection<CardTransactionRecord> =
        db.selectFrom(T)
            .orderBy(T.DT_CREATED.desc())
            .fetch()

    companion object {
        val T = CARD_TRANSACTION!!
    }
}