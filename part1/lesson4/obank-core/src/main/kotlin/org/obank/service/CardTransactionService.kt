package org.obank.service

import org.obank.model.*
import org.obank.repository.CardRepo
import org.obank.repository.CardTransactionRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.util.*

@Service
class CardTransactionService {

    @Autowired
    private lateinit var repo: CardTransactionRepo

    @Autowired
    private lateinit var cardRepo: CardRepo

    // Получить историю операций по счёту для пользователя
    fun getHistory(
        customerId: String,
        accountNumber: String,
        dateFrom: LocalDate,
        dateUntil: LocalDate
    ): List<TransactionHistoryItem> {

        val card = cardRepo.findByAccountNumber(accountNumber)
        when {
            card == null || card.ownerId == null -> throw ResourceNotFoundException("карта/счёт не найдены")
            card.ownerId.toString() != customerId -> throw InvalidAccessException("нарушение прав доступа")
        }
        card!!

        return repo.findByCardAndDtRange(
            card.id,
            dtFrom = dateFrom.atStartOfDay(),
            dtTill = dateUntil.atStartOfDay().plusDays(1)
        ).map {
            it.toTransactionHistoryItem(card.accountNumber)
        }
    }

    // Получить детальную информацию о транзакции
    fun getDetails(customerId: String, transactionId: String): TransactionDetails {
        val tran = repo.getById(UUID.fromString(transactionId))
        val card = cardRepo.getById(tran.cardId)
        if (card.ownerId.toString() != customerId) {
            throw InvalidAccessException("нарушение прав доступа")
        }
        return tran.toTransactionDetails(card.accountNumber)
    }

}
