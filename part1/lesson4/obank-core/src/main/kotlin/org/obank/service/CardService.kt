package org.obank.service

import mu.KotlinLogging
import org.obank.model.BankingCard
import org.obank.model.toBankingCard
import org.obank.repository.CardRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
@Transactional
class CardService {

    private val log = KotlinLogging.logger { }

    @Autowired
    private lateinit var repo: CardRepo

    // Получить список карт для пользователя
    fun findBankingCardsByCustomer(customerId: String): Collection<BankingCard> =
        repo.findByOwner(UUID.fromString(customerId))
            .map { it.toBankingCard(customerId) }
}
