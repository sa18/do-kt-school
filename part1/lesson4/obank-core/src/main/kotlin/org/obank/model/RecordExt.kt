package org.obank.model

import com.talanlabs.avatargenerator.cat.CatAvatar
import org.obank.Util
import org.obank.db.schema_public.tables.records.CardRecord
import org.obank.db.schema_public.tables.records.CardTransactionRecord
import org.obank.db.schema_public.tables.records.CustomerRecord
import org.obank.db.schema_public.tables.records.SmsLogRecord
import java.io.ByteArrayOutputStream
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*
import javax.imageio.ImageIO

object SmsLogRecordBuilder {
    fun buildForAuthCode(
        customerId: UUID,
        mobilePhone: String,
        authCode: String,
        authToken: String
    ) = SmsLogRecord().also {
        it.customerId = customerId
        it.destination = mobilePhone
        it.authToken = authToken
        it.authCode = authCode
        it.dtCreated = LocalDateTime.now()
        it.dtSent = it.dtCreated
        it.dtExpire = it.dtCreated.plusMinutes(1)
        //it.content = Util.transliterate("Проверочный код $authCode. Никому его не говорите!")
        it.content = authCode
    }
}

val CardRecord.isExpired
    get() = dateExpire.isBefore(LocalDate.now())

fun CardRecord.toBankingCard(customerId: String?): BankingCard =
    BankingCard(
        cardNumber = paymentNumber,
        accountNumber = paymentNumber,
        dateValidTill = dateExpire,
        ownerCustomerId = customerId
    )

fun CustomerRecord.toCustomerProfile() =
    CustomerProfile(
        id = id.toString(),
        valid = isValid,
        fio = fio,
        dtCreated = dtCreated,
        email = email,
        mobilePhone = mobilePhone
    )

fun CustomerRecord.generatePic() {
    val defaultPic = CatAvatar.newAvatarBuilder()
        .build()
        .create(Math.random().hashCode().toLong())
    val os = ByteArrayOutputStream()
    ImageIO.write(defaultPic, "PNG", os)
    setPic(*os.toByteArray())
}

val CardTransactionRecord.canBeCanceled: Boolean
    get() = listOf(TransactionStatus.ENTERING, TransactionStatus.PENDING).map { it.toString() }.contains(this.status)

fun CardTransactionRecord.toTransactionHistoryItem(accountNumber: String) =
    TransactionHistoryItem(
        id = id.toString(),
        accountNumber = accountNumber,
        dt = dtCreated,
        debitCredit = dc.toDebitCredit(),
        status = TransactionStatus.valueOf(status),
        amount = amount.toFloat(),
        currency = currency,
        shortDescription = shortDesc
    )

fun CardTransactionRecord.toTransactionDetails(accountNumber: String) =
    TransactionDetails(
        id = id.toString(),
        accountNumber = accountNumber,
        dt = dtCreated,
        debitCredit = dc.toDebitCredit(),
        status = TransactionStatus.valueOf(status),
        amount = amount.toFloat(),
        currency = currency,
        shortDescription = shortDesc,
        fullDescription = fullDesc,
        otherSideAccountNumber = otherSideAccountNumber,
        otherSideName = otherSideName,
        canBeCanceled = canBeCanceled
    )

inline fun String.toDebitCredit(): DebitCredit =
    when (this) {
        "D" -> DebitCredit.DEBIT
        "C" -> DebitCredit.CREDIT
        else -> throw IllegalArgumentException()
    }