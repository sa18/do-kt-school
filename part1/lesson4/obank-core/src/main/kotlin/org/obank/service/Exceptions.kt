package org.obank.service

class ResourceNotFoundException(msg: String = "Not found") : RuntimeException(msg)
class InvalidAccessException(msg: String = "Invalid access") : RuntimeException(msg)
class AuthenticationException(msg: String = "Authentication required") : RuntimeException(msg)