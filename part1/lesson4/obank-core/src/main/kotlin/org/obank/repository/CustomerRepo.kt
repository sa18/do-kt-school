package org.obank.repository

import org.jooq.DSLContext
import org.jooq.impl.DSL
import org.obank.db.schema_public.Tables.CUSTOMER
import org.obank.db.schema_public.tables.records.CustomerRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class CustomerRepo {

    @Autowired
    private lateinit var db: DSLContext

    fun findValidByMobilePhone(mobilePhone: String): CustomerRecord? =
        db.selectFrom(CUSTOMER)
            .where(CUSTOMER.IS_VALID)
            .and(CUSTOMER.MOBILE_PHONE.eq(mobilePhone))
            .fetchAny()

    fun insertNew(customer: CustomerRecord) {
        customer.id = UUID.randomUUID()
        db.executeInsert(customer)
    }

    fun getById(customerId: UUID): CustomerRecord =
        db.selectFrom(CUSTOMER)
            .where(CUSTOMER.ID.eq(customerId))
            .fetchSingle()

    fun updateIsValid(customerId: UUID, isValid: Boolean) {
        db.update(CUSTOMER)
            .set(CUSTOMER.IS_VALID, isValid)
            .set(CUSTOMER.DT_UPDATED, DSL.currentLocalDateTime())
            .where(CUSTOMER.ID.eq(customerId))
            .execute()
    }

    fun findAll() : Collection<CustomerRecord> =
        db.selectFrom(CUSTOMER)
            .orderBy(CUSTOMER.DT_CREATED)
            .fetch()

    companion object {
        private val allFieldsWithoutPic = CUSTOMER.fields().filter { it.name != CUSTOMER.PIC.name }
    }
}
