package org.obank.repository

import org.jooq.DSLContext
import org.jooq.impl.DSL
import org.obank.db.schema_public.Tables.CHAT_EVENT
import org.obank.db.schema_public.tables.records.ChatEventRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import java.time.LocalDateTime
import java.util.*

@Repository
class ChatEventRepo {

    @Autowired
    private lateinit var db: DSLContext

    fun insertNew(chatEventRecord: ChatEventRecord) {
        chatEventRecord.id = UUID.randomUUID()
        db.executeInsert(chatEventRecord)
    }

    fun findByCustomerAndDtCreatedFrom(customerId: UUID, dtCreated: LocalDateTime): List<ChatEventRecord> =
        db.selectFrom(CHAT_EVENT)
            .where(CHAT_EVENT.CUSTOMER_ID.eq(customerId))
            .and(CHAT_EVENT.DT_CREATED.greaterOrEqual(dtCreated))
            .orderBy(CHAT_EVENT.DT_CREATED)
            .fetchInto(ChatEventRecord::class.java)

    fun findByDtCreatedWithinLastDays_dtOtherSideShownIsNull_clientIdIsNotNull(lastDays: Int): List<ChatEventRecord> =
        db.selectFrom(CHAT_EVENT)
            .where(CHAT_EVENT.DT_CREATED.greaterOrEqual(LocalDateTime.now().minusDays(lastDays.toLong())))
            .and(CHAT_EVENT.CLIENT_ID.isNotNull)
            .and(CHAT_EVENT.DT_OTHER_SIDE_SHOWN.isNull) // isNew
            .orderBy(CHAT_EVENT.DT_CREATED)
            .fetchInto(ChatEventRecord::class.java)

    fun setDtOtherSideShownToNow(id: UUID) {
        db.update(CHAT_EVENT)
            .set(CHAT_EVENT.DT_OTHER_SIDE_SHOWN, DSL.currentLocalDateTime())
            .where(CHAT_EVENT.ID.eq(id))
            .execute()
    }
}
