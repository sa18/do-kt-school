package org.obank.repository

import org.jooq.DSLContext
import org.obank.db.schema_public.Tables.CARD
import org.obank.db.schema_public.tables.records.CardRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class CardRepo {

    @Autowired
    private lateinit var db: DSLContext

    fun insertNew(c: CardRecord) {
        c.id = UUID.randomUUID()
        db.executeInsert(c)
    }

    fun findByAccountNumber(accountNumber: String): CardRecord? =
        db.selectFrom(CARD)
            .where(CARD.ACCOUNT_NUMBER.eq(accountNumber))
            .fetchAny()

    fun findByPaymentNumberForUpdate(cardNumber: String): CardRecord? =
        db.selectFrom(CARD)
            .where(CARD.PAYMENT_NUMBER.eq(cardNumber))
            .forUpdate()
            .fetchAny()

    fun updateOwner(cardId: UUID, customerId: UUID) =
        db.update(CARD)
            .set(CARD.OWNER_ID, customerId)
            .where(CARD.ID.eq(cardId))
            .execute()

    fun findByOwner(customerId: UUID): Collection<CardRecord> =
        db.selectFrom(CARD)
            .where(CARD.OWNER_ID.eq(customerId))
            .fetchInto(CardRecord::class.java)

    fun getById(cardId: UUID): CardRecord =
        db.selectFrom(CARD)
            .where(CARD.ID.eq(cardId))
            .fetchSingle()

    fun findAll(): Collection<CardRecord> =
        db.selectFrom(CARD)
            .orderBy(CARD.DT_CREATED.desc())
            .fetchInto(CardRecord::class.java)
}
