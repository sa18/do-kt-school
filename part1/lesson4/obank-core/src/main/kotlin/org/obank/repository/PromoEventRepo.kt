package org.obank.repository

import org.jooq.DSLContext
import org.obank.db.schema_public.Tables.PROMO_EVENT
import org.obank.db.schema_public.tables.records.PromoEventRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class PromoEventRepo {

    @Autowired
    private lateinit var db: DSLContext

    fun insertNew(v: PromoEventRecord) {
        v.id = UUID.randomUUID()
        db.executeInsert(v)
    }

    fun findActualWithoutCustomer(): List<PromoEventRecord> =
        db.selectFrom(PROMO_EVENT)
            .where(PROMO_EVENT.CUSTOMER_ID.isNull)
            .and(PROMO_EVENT.IS_ACTUAL.isTrue)
            .orderBy(PROMO_EVENT.DT_CREATED)
            .fetchInto(PromoEventRecord::class.java)

    fun findActualByCustomer(customerId: UUID): List<PromoEventRecord> =
        db.selectFrom(PROMO_EVENT)
            .where(PROMO_EVENT.CUSTOMER_ID.eq(customerId))
            .and(PROMO_EVENT.IS_ACTUAL.isTrue)
            .orderBy(PROMO_EVENT.DT_CREATED)
            .fetchInto(PromoEventRecord::class.java)

}
