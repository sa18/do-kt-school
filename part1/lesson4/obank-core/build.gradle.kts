import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootJar

plugins {
    kotlin("jvm")
    kotlin("plugin.spring").version("1.3.50")
    id("org.springframework.boot") version "2.1.9.RELEASE"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":lesson4:obank-db"))
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))
    implementation("org.springframework.boot:spring-boot-starter-jooq:2.+") {
        exclude(group = "org.jooq")
    }
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor:2.+")
    implementation("io.github.microutils:kotlin-logging:1.7+")
    testImplementation("ch.qos.logback:logback-classic")
    implementation("org.simpleflatmapper:sfm-jooq:7.+")
    implementation("org.simpleflatmapper:sfm-csv:7.+")
    // JUnit
    testImplementation("org.springframework.boot:spring-boot-starter-test:2.+") {
        exclude(module = "junit")
    }
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.+")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.+")
    testImplementation("com.willowtreeapps.assertk:assertk-jvm:0.20")
    // Mockito
    testImplementation("org.mockito:mockito-core:2.+")
    testImplementation("io.mockk:mockk:1.9.+")
    // JSoup
    implementation("org.jsoup:jsoup:1.+")
    // Avatar generator
    implementation("com.talanlabs:avatar-generator-cat:1.+")
}

tasks.getByName<Jar>("jar") {
    enabled = true
}

tasks.getByName<BootJar>("bootJar") {
    enabled = false
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        suppressWarnings = true
        jvmTarget = "1.8"
    }
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform { }
}
