import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootJar

plugins {
    val kotlinVersion = "1.3.50"
    kotlin("jvm")
    kotlin("plugin.spring").version(kotlinVersion)
    id("org.springframework.boot") version "2.1.9.RELEASE"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":lesson4:obank-db"))
    implementation(project(":lesson4:obank-core"))
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))
    implementation("org.springframework.boot:spring-boot-starter-jooq:2.+"){
        exclude(group = "org.jooq")
    }
    implementation("org.springframework.boot:spring-boot-starter-web:2.+")
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor:2.+")
    //runtimeOnly("org.springframework.boot:spring-boot-devtools")
    implementation("io.github.microutils:kotlin-logging:1.7+")
    implementation("ch.qos.logback:logback-classic")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.+")
    implementation("io.springfox:springfox-swagger-ui:2.+")
    implementation("io.springfox:springfox-swagger2:2.+")
    // Jooq
    implementation("org.jooq:jooq:3.+")
    implementation("org.jooq:jooq-meta:3.+")
    // JUnit
    testImplementation("org.springframework.boot:spring-boot-starter-test:2.+") {
        exclude(module = "junit")
    }
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.+")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.+")
    testImplementation("com.willowtreeapps.assertk:assertk-jvm:0.20")
    // Mockito
    testImplementation("org.mockito:mockito-core:2.+")
    testImplementation("io.mockk:mockk:1.9.+")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        suppressWarnings = true
        jvmTarget = "1.8"
    }
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform { }
}
