package org.obank.webapi

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.obank.service.AuthenticationException
import org.obank.service.CardService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("account")
@Api(value = "account", description = "Управление счетами и картами")
class AccountController {

    @Autowired
    private lateinit var cardService: CardService

    @Autowired
    private lateinit var loginInfo: LoginInfo

    @GetMapping("/cardsList")
    @ApiOperation("Получить список банковских карт для текущего пользователя")
    fun getCardsListForCurrentUser() : AccountGetCardsListResponse {
        if (!loginInfo.isLoggedIn) throw AuthenticationException()
        return AccountGetCardsListResponse(
            cardsList = cardService.findBankingCardsByCustomer(loginInfo.customerId!!)
        )
    }

}