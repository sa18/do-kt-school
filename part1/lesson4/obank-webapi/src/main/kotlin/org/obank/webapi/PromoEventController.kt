package org.obank.webapi

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.obank.service.AuthenticationException
import org.obank.service.PromoEventService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("promo-event")
@Api("Сервисы промо-акций")
class PromoEventController {

    @Autowired
    private lateinit var promoEventService: PromoEventService

    @Autowired
    private lateinit var loginInfo: LoginInfo

    @ApiOperation("Получить историю сообщений для текущего пользователя")
    @PostMapping("/items")
    fun getItemsList(@RequestBody request: PromoGetItemsRequest) : PromoGetItemsResponse {
        if (!loginInfo.isLoggedIn) throw AuthenticationException()
        return promoEventService.findActualPromoEvents(loginInfo.customerId!!)
            .let {
                PromoGetItemsResponse(items = it)
            }
    }
}