package org.obank.webapi

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.obank.service.AuthenticationException
import org.obank.service.CustomerAuthService
import org.obank.service.CustomerProfileService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("customer")
@Api(description = "Регистрация, авторизация и управление профилем пользователя")
class CustomerController {

    @Autowired
    private lateinit var customerAuthService: CustomerAuthService

    @Autowired
    private lateinit var customerProfileService: CustomerProfileService

    @Autowired
    private lateinit var loginInfo: LoginInfo

    @PostMapping("/register")
    @ApiOperation("Зарегистрировать нового пользователя и привязать его к банковской карте")
    fun registerNew(@RequestBody request: CustomerRegisterRequest): CustomerRegisterResponse {
        customerAuthService.registerNew(request.cardNumber, request.fio, request.mobilePhone, request.email)
        loginInfo.invalidate()
        return CustomerRegisterResponse("OK")
    }

    @GetMapping("/auth/begin")
    @ApiOperation("Начало авторизации пользователя (высылает SMS на мобильный номер)")
    fun authBegin(request: CustomerAuthBeginRequest): CustomerAuthBeginResponse {
        val token = customerAuthService.authBegin(request.mobilePhone)
        loginInfo.invalidate()
        return CustomerAuthBeginResponse(token, "OK")
    }

    @GetMapping("/auth/verify")
    @ApiOperation("Верификация пользователя через отправленное SMS")
    fun authVerify(request: CustomerAuthVerifyRequest): CustomerAuthVerifyResponse {
        val customerId = customerAuthService.authVerify(request.token, request.shortCode)
        loginInfo.customerId = customerId
        return CustomerAuthVerifyResponse(0, 0, "OK")
    }

    @GetMapping("/profile")
    @ApiOperation("Получить профиль пользователя")
    fun getProfile(): CustomerGetProfileResponse {
        if (!loginInfo.isLoggedIn)
            throw AuthenticationException()
        val profile = customerProfileService.getProfile(loginInfo.customerId!!)
        return CustomerGetProfileResponse(
            id = profile.id,
            fio = profile.fio,
            dtCreated = profile.dtCreated,
            email = profile.email
        )
    }

    @GetMapping("/avatar", produces = [MediaType.IMAGE_PNG_VALUE])
    @ApiOperation("Получить картинку пользователя")
    @ResponseBody
    fun getAvatar(/*request: CustomerGetAvatarRequest*/): ByteArray {
        if (!loginInfo.isLoggedIn)
            throw AuthenticationException()
        return customerProfileService.getUserpic(loginInfo.customerId!!)
    }

    @PutMapping("/profile")
    @ApiOperation("Изменить профиль пользователя")
    fun editProfile(request: CustomerChangeProfileRequest) {
        if (!loginInfo.isLoggedIn)
            throw AuthenticationException()
        customerProfileService.edit(
            customerId = loginInfo.customerId!!,
            fio = request.fio,
            userPic = request.avatar,
            email = request.email
        )
    }
}
