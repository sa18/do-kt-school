package org.obank.webapi

import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Component
import org.springframework.web.context.WebApplicationContext

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
class LoginInfo(var customerId: String? = null) {
    val isLoggedIn: Boolean
        get() = customerId != null

    fun invalidate() {
        customerId = null
    }
}