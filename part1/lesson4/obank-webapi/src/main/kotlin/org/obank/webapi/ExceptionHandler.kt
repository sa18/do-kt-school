package org.obank.webapi

import mu.KotlinLogging
import org.obank.service.AuthenticationException
import org.obank.service.InvalidAccessException
import org.obank.service.ResourceNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDateTime

@ControllerAdvice(annotations = [RestController::class])
class ExceptionHandler {

    private val log = KotlinLogging.logger { }

    @ExceptionHandler(ResourceNotFoundException::class)
    fun handle(x: ResourceNotFoundException): ResponseEntity<ApiError> {
        log.error { x }
        log.debug(null, x)
        return ApiError(x, HttpStatus.NOT_FOUND).toResponseEntity()
    }

    @ExceptionHandler(InvalidAccessException::class)
    fun handle(x: InvalidAccessException): ResponseEntity<ApiError> {
        log.error { x }
        log.debug(null, x)
        return ApiError(x, HttpStatus.FORBIDDEN).toResponseEntity()
    }

    @ExceptionHandler(AuthenticationException::class)
    fun handle(x: AuthenticationException): ResponseEntity<ApiError> {
        log.error { x }
        log.debug(null, x)
        return ApiError(x, HttpStatus.UNAUTHORIZED).toResponseEntity()
    }

    @ExceptionHandler(Exception::class)
    fun handle(x: Exception): ResponseEntity<ApiError> {
        log.error { x }
        log.debug(null, x)
        return ApiError(x, HttpStatus.INTERNAL_SERVER_ERROR).toResponseEntity()
    }
}

class ApiError(x: Exception, private val httpStatus: HttpStatus) {
    val status = httpStatus.value()
    val error = httpStatus.name
    val message = x.localizedMessage
    val timestamp: LocalDateTime = LocalDateTime.now()
    fun toResponseEntity() : ResponseEntity<ApiError> {
        return ResponseEntity(this, httpStatus)
    }
}
