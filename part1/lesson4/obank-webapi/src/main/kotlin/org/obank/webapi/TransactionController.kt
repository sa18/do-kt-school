package org.obank.webapi

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.obank.service.AuthenticationException
import org.obank.service.CardTransactionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("transaction")
@Api(description = "Операции с транзакциями")
class TransactionController {

    @Autowired
    private lateinit var cardTransactionService: CardTransactionService

    @Autowired
    private lateinit var loginInfo: LoginInfo

    @PostMapping("/history")
    @ApiOperation("Получить историю транзакций для текущего пользователя")
    fun getHistory(@RequestBody request: TransactionGetHistoryRequest): TransactionGetHistoryResponse {
        if (!loginInfo.isLoggedIn) throw AuthenticationException()
        return cardTransactionService.getHistory(
            customerId = loginInfo.customerId!!,
            accountNumber = request.accountNumber,
            dateFrom = request.dateFrom,
            dateUntil = request.dateTillInclusive.plusDays(1)
        ).let {
            TransactionGetHistoryResponse(it)
        }
    }

    @GetMapping("/details")
    @ApiOperation("Получить детальную информацию по транзакции")
    fun getDetails(transactionId: String): TransactionDetailsResponse {
        if (!loginInfo.isLoggedIn) throw AuthenticationException()
        return cardTransactionService.getDetails(loginInfo.customerId!!, transactionId)
            .let {
                TransactionDetailsResponse(it)
            }
    }

}