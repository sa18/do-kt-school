package org.obank.webapi

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import mu.KotlinLogging
import org.obank.model.*
import org.obank.repository.CardRepo
import org.obank.repository.CardTransactionRepo
import org.obank.repository.CustomerRepo
import org.obank.service.BankSimulatorService
import org.obank.service.CustomerProfileService
import org.obank.service.ResourceNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("simulator")
@Api(value = "simulator", description = "Симуляционные сервисы (для демо)")
@CrossOrigin
class SimulatorController {

    private val log = KotlinLogging.logger {}

    @Autowired
    private lateinit var simulatorService: BankSimulatorService

    @Autowired
    private lateinit var customerRepo: CustomerRepo

    @Autowired
    private lateinit var customerProfileService: CustomerProfileService

    @Autowired
    private lateinit var cardRepo: CardRepo

    @Autowired
    private lateinit var cardTransactionRepo: CardTransactionRepo

    @Autowired
    private lateinit var loginInfo: LoginInfo

    @GetMapping("/issueCards")
    @ApiOperation("Симулировать выпуск N банковских карточек (возвращает список их номеров)")
    fun issueCards(@RequestParam n: Int): List<String> {
        return simulatorService.issueBankingCards(n).map { it.paymentNumber }
    }

    @GetMapping("/listAllCards")
    @ApiOperation("Вернуть список всех банковских карточек")
    fun listAllCards(): Collection<BankingCard> {
        return cardRepo.findAll().map { it.toBankingCard(it.ownerId?.toString()) }
    }

    @GetMapping("/listAllCustomers")
    @ApiOperation("Вернуть список всех пользователей")
    fun listAllCustomers(): Collection<CustomerProfile> {
        return customerRepo.findAll().map { it.toCustomerProfile() }
    }

    @GetMapping("/listAllCardTransactions")
    @ApiOperation("Вернуть список всех транзакций")
    fun listAllCardTransactions(): Collection<TransactionHistoryItem> {
        return cardTransactionRepo.findAll().map { it.toTransactionHistoryItem("") }
    }

    @GetMapping("/login")
    @ApiOperation("Симулировать вход пользователя (успешную авторизацию) в текущую HTTP-сессию")
    fun loginByMobilePhone(mobilePhone: String) {
        val customer = customerRepo.findValidByMobilePhone(mobilePhone.trim())
            ?: throw ResourceNotFoundException("пользователь с номером телефона не найден")
        log.info("Force login user id = ${customer.id}")
        loginInfo.customerId = customer.id.toString()
    }

    @GetMapping("/logout")
    @ApiOperation("Выйти текущему пользователю")
    fun logout(mobilePhone: String) {
        log.info("Force logout user id = ${loginInfo.customerId}")
        loginInfo.invalidate()
    }

    @GetMapping("/invalidateUser")
    @ApiOperation("Сделать пользователя неактивным")
    fun invalidateUser(mobilePhone: String) {
        val customer = customerRepo.findValidByMobilePhone(mobilePhone.trim())
            ?: throw ResourceNotFoundException("пользователь с номером телефона не найден")

        customerProfileService.invalidateCustomer(customer.id.toString())
        if (loginInfo.isLoggedIn && loginInfo.customerId==customer.id.toString()) {
            loginInfo.invalidate()
        }
    }

}
