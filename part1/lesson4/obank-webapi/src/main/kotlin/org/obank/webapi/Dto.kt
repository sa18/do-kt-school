package org.obank.webapi

import org.obank.model.*
import java.time.LocalDate
import java.time.LocalDateTime

data class CustomerRegisterRequest(
    val cardNumber: String,
    val fio: String,
    val mobilePhone: String,
    val email: String
)

data class CustomerRegisterResponse(val textualAnswer: String)

data class CustomerAuthBeginRequest(val mobilePhone: String)
data class CustomerAuthBeginResponse(val token: String, val textualAnswer: String)

data class CustomerAuthVerifyRequest(val token: String, val shortCode: String)
data class CustomerAuthVerifyResponse(
    val numNewChatMessages: Int,
    val numNewPersonalPromoItems: Int,
    val textualAnswer: String
)

data class CustomerGetProfileResponse(val id: String, val fio: String, val dtCreated: LocalDateTime, val email: String)

data class CustomerGetAvatarRequest(val size: String /* small | large */)

data class CustomerChangeProfileRequest(
    var fio: String? = null,
    var email: String? = null,
    var avatar: ByteArray? = null
)

data class AccountGetCardsListResponse(val cardsList: Collection<BankingCard>)

data class TransactionGetHistoryRequest(
    val accountNumber: String,
    val dateFrom: LocalDate,
    val dateTillInclusive: LocalDate
)

data class TransactionGetHistoryResponse(val items: Collection<TransactionHistoryItem>)

data class TransactionDetailsResponse(val details: TransactionDetails)

data class ChatGetHistoryRequest(val dateFrom: LocalDate)
data class ChatGetHistoryResponse(val messagesList: List<ChatMessage>)

data class ChatPostMessageRequest(val clientSideMessageId: String, val messageText: String)
data class ChatPostMessageResponse(val clientSideMessageId: String, val serverSideMessageId: String)

data class PromoGetItemsRequest(val newOnly: Boolean = false, val dateFrom: LocalDate)
data class PromoGetItemsResponse(val items: List<PromoItem>)
