package org.obank.webapi

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.obank.service.AuthenticationException
import org.obank.service.ChatService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("chat")
@Api("Сервисы чата с банком")
class ChatController {

    @Autowired
    private lateinit var chatService: ChatService

    @Autowired
    private lateinit var loginInfo: LoginInfo

    @ApiOperation("Получить историю сообщений для текущего пользователя")
    @PostMapping("/history")
    fun getHistory(@RequestBody request: ChatGetHistoryRequest) : ChatGetHistoryResponse {
        if (!loginInfo.isLoggedIn) throw AuthenticationException()
        return chatService.getChatHistory(loginInfo.customerId!!, request.dateFrom)
            .let {
                ChatGetHistoryResponse(it)
            }
    }

    @ApiOperation("Отправить сообщение от текущего пользователя")
    @PostMapping("message")
    fun postMessage(@RequestBody request: ChatPostMessageRequest) : ChatPostMessageResponse {
        if (!loginInfo.isLoggedIn) throw AuthenticationException()
        return chatService.sendNewMessageFromCustomer(loginInfo.customerId!!, request.clientSideMessageId, request.messageText)
            .let {
                ChatPostMessageResponse(clientSideMessageId = request.clientSideMessageId, serverSideMessageId = it)
            }
    }
}