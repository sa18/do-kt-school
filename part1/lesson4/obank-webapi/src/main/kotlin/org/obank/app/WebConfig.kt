package org.obank.app

import com.google.common.collect.ImmutableList
import org.obank.webapi.CustomerController
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.method.HandlerTypePredicate
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@Configuration
@ComponentScan(basePackageClasses = [CustomerController::class])
@EnableSwagger2
class WebConfig : WebMvcConfigurer {

    private val apiVersion = "v1"
    private val urlPrefix = "api/$apiVersion"

    override fun configurePathMatch(configurer: PathMatchConfigurer) {
        configurer.addPathPrefix(urlPrefix, HandlerTypePredicate.forAnnotation(RestController::class.java))
    }

    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
            .allowedOrigins("*")
            .allowedMethods("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH")
            .allowCredentials(true)
    }

    @Bean
    fun corsSource(): CorsConfigurationSource {
        val configuration = CorsConfiguration().apply {
            allowedOrigins = ImmutableList.of("*")
            allowedMethods = ImmutableList.of("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH")
            allowCredentials = true
            allowedHeaders = ImmutableList.of("Authorization", "Cache-Control", "Content-Type")
        }
        return UrlBasedCorsConfigurationSource().apply {
            registerCorsConfiguration("/**", configuration)
        }
    }

    @Bean
    fun api(): Docket {
        val apiInfo = ApiInfoBuilder()
            .title("OBank REST API")
            .description("October Bank - пример бэкенд-приложения на Kotlin и Spring")
            .version(apiVersion)
            .build()
        return Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage(CustomerController::class.java.`package`.name))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo)
    }

}
