package org.obank.test

import org.obank.app.WebConfig
import org.obank.config.CoreConfig
import org.obank.config.DatabaseConfig
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@EnableAutoConfiguration
@Import(CoreConfig::class, DatabaseConfig::class, WebConfig::class)
class ObankApplicationTest {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(ObankApplicationTest::class.java, *args)
        }
    }

}
