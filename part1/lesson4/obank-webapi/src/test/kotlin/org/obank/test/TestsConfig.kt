package org.obank.test

import org.obank.app.WebConfig
import org.obank.config.CoreConfig
import org.obank.config.DatabaseConfig
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.web.servlet.config.annotation.EnableWebMvc

@Configuration
@EnableAutoConfiguration
@Import(CoreConfig::class, DatabaseConfig::class, WebConfig::class)
class TestsConfig
