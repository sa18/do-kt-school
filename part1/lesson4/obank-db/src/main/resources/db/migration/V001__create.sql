CREATE TABLE customer
(
    id           UUID        NOT NULL DEFAULT RANDOM_UUID() PRIMARY KEY,
    dt_created   TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    dt_updated   TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    is_valid     BOOLEAN     NOT NULL,
    mobile_phone VARCHAR(20) NOT NULL,
    fio          VARCHAR(80) NOT NULL,
    email        VARCHAR(80) NOT NULL,
    pic          MEDIUMBLOB
);

CREATE TABLE card
(
    id             UUID        NOT NULL DEFAULT RANDOM_UUID() PRIMARY KEY,
    dt_created     TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    date_expire    DATE        NOT NULL,
    payment_number VARCHAR(80) NOT NULL,
    account_number VARCHAR(50) NOT NULL,
    owner_id       UUID        NULL,
    comment        VARCHAR(255),
    FOREIGN KEY (owner_id) REFERENCES customer (id)
);

CREATE TABLE card_transaction
(
    id                        UUID           NOT NULL DEFAULT RANDOM_UUID() PRIMARY KEY,
    dt_created                TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    dt_updated                TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    dc                        CHAR(1)        NOT NULL,
    status                    VARCHAR(20)    NOT NULL,
    amount                    DECIMAL(20, 4) NOT NULL,
    currency                  CHAR(3)        NOT NULL,
    short_desc                VARCHAR(80),
    full_desc                 VARCHAR(1024),
    card_id                   UUID           NOT NULL,
    linked_transaction_id     UUID           NULL,
    other_side_account_number VARCHAR(20),
    other_side_name           VARCHAR(255),
    FOREIGN KEY (card_id) REFERENCES card (id),
    FOREIGN KEY (linked_transaction_id) REFERENCES card_transaction (id)
);

CREATE TABLE chat_event
(
    id                      UUID        NOT NULL DEFAULT RANDOM_UUID() PRIMARY KEY,
    client_id               VARCHAR(80),
    type                    VARCHAR(20) NOT NULL,
    dt_created              TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    customer_id             UUID        NOT NULL,
    text                    TEXT        NOT NULL,
    dt_other_side_delivered TIMESTAMP   NULL,
    dt_other_side_shown     TIMESTAMP   NULL,
    FOREIGN KEY (customer_id) REFERENCES customer (id)
);

CREATE TABLE promo_event
(
    id          UUID          NOT NULL DEFAULT RANDOM_UUID() PRIMARY KEY,
    dt_created  TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    category    VARCHAR(20)   NOT NULL,
    is_actual   BOOLEAN       NOT NULL,
    is_new      BOOLEAN       NOT NULL,
    is_personal BOOLEAN       NOT NULL,
    customer_id UUID,
    title       VARCHAR(1024) NOT NULL,
    FOREIGN KEY (customer_id) REFERENCES customer (id)
);

CREATE TABLE sms_log
(
    id           UUID      NOT NULL DEFAULT RANDOM_UUID() PRIMARY KEY,
    dt_created   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    dt_sent      TIMESTAMP,
    dt_expire    DATETIME,
    dt_validated TIMESTAMP,
    auth_token   VARCHAR(20),
    auth_code    VARCHAR(20),
    customer_id  UUID,
    destination  VARCHAR(20),
    content      VARCHAR(255),
    FOREIGN KEY (customer_id) REFERENCES customer (id)
);
