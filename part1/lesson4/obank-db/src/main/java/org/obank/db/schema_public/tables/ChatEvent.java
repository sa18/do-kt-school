/*
 * This file is generated by jOOQ.
 */
package org.obank.db.schema_public.tables;


import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row8;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;
import org.obank.db.schema_public.DefaultSchema;
import org.obank.db.schema_public.Indexes;
import org.obank.db.schema_public.Keys;
import org.obank.db.schema_public.tables.records.ChatEventRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.1"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ChatEvent extends TableImpl<ChatEventRecord> {

    private static final long serialVersionUID = 1288884256;

    /**
     * The reference instance of <code>CHAT_EVENT</code>
     */
    public static final ChatEvent CHAT_EVENT = new ChatEvent();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<ChatEventRecord> getRecordType() {
        return ChatEventRecord.class;
    }

    /**
     * The column <code>CHAT_EVENT.ID</code>.
     */
    public final TableField<ChatEventRecord, UUID> ID = createField(DSL.name("ID"), org.jooq.impl.SQLDataType.UUID.nullable(false).defaultValue(org.jooq.impl.DSL.field("RANDOM_UUID()", org.jooq.impl.SQLDataType.UUID)), this, "");

    /**
     * The column <code>CHAT_EVENT.CLIENT_ID</code>.
     */
    public final TableField<ChatEventRecord, String> CLIENT_ID = createField(DSL.name("CLIENT_ID"), org.jooq.impl.SQLDataType.VARCHAR(80), this, "");

    /**
     * The column <code>CHAT_EVENT.TYPE</code>.
     */
    public final TableField<ChatEventRecord, String> TYPE = createField(DSL.name("TYPE"), org.jooq.impl.SQLDataType.VARCHAR(20).nullable(false), this, "");

    /**
     * The column <code>CHAT_EVENT.DT_CREATED</code>.
     */
    public final TableField<ChatEventRecord, LocalDateTime> DT_CREATED = createField(DSL.name("DT_CREATED"), org.jooq.impl.SQLDataType.LOCALDATETIME.nullable(false).defaultValue(org.jooq.impl.DSL.field("CURRENT_TIMESTAMP", org.jooq.impl.SQLDataType.LOCALDATETIME)), this, "");

    /**
     * The column <code>CHAT_EVENT.CUSTOMER_ID</code>.
     */
    public final TableField<ChatEventRecord, UUID> CUSTOMER_ID = createField(DSL.name("CUSTOMER_ID"), org.jooq.impl.SQLDataType.UUID.nullable(false), this, "");

    /**
     * The column <code>CHAT_EVENT.TEXT</code>.
     */
    public final TableField<ChatEventRecord, String> TEXT = createField(DSL.name("TEXT"), org.jooq.impl.SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column <code>CHAT_EVENT.DT_OTHER_SIDE_DELIVERED</code>.
     */
    public final TableField<ChatEventRecord, LocalDateTime> DT_OTHER_SIDE_DELIVERED = createField(DSL.name("DT_OTHER_SIDE_DELIVERED"), org.jooq.impl.SQLDataType.LOCALDATETIME, this, "");

    /**
     * The column <code>CHAT_EVENT.DT_OTHER_SIDE_SHOWN</code>.
     */
    public final TableField<ChatEventRecord, LocalDateTime> DT_OTHER_SIDE_SHOWN = createField(DSL.name("DT_OTHER_SIDE_SHOWN"), org.jooq.impl.SQLDataType.LOCALDATETIME, this, "");

    /**
     * Create a <code>CHAT_EVENT</code> table reference
     */
    public ChatEvent() {
        this(DSL.name("CHAT_EVENT"), null);
    }

    /**
     * Create an aliased <code>CHAT_EVENT</code> table reference
     */
    public ChatEvent(String alias) {
        this(DSL.name(alias), CHAT_EVENT);
    }

    /**
     * Create an aliased <code>CHAT_EVENT</code> table reference
     */
    public ChatEvent(Name alias) {
        this(alias, CHAT_EVENT);
    }

    private ChatEvent(Name alias, Table<ChatEventRecord> aliased) {
        this(alias, aliased, null);
    }

    private ChatEvent(Name alias, Table<ChatEventRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> ChatEvent(Table<O> child, ForeignKey<O, ChatEventRecord> key) {
        super(child, key, CHAT_EVENT);
    }

    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.CONSTRAINT_INDEX_4, Indexes.PRIMARY_KEY_4);
    }

    @Override
    public UniqueKey<ChatEventRecord> getPrimaryKey() {
        return Keys.CONSTRAINT_4;
    }

    @Override
    public List<UniqueKey<ChatEventRecord>> getKeys() {
        return Arrays.<UniqueKey<ChatEventRecord>>asList(Keys.CONSTRAINT_4);
    }

    @Override
    public List<ForeignKey<ChatEventRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<ChatEventRecord, ?>>asList(Keys.CONSTRAINT_44);
    }

    public Customer customer() {
        return new Customer(this, Keys.CONSTRAINT_44);
    }

    @Override
    public ChatEvent as(String alias) {
        return new ChatEvent(DSL.name(alias), this);
    }

    @Override
    public ChatEvent as(Name alias) {
        return new ChatEvent(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public ChatEvent rename(String name) {
        return new ChatEvent(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public ChatEvent rename(Name name) {
        return new ChatEvent(name, null);
    }

    // -------------------------------------------------------------------------
    // Row8 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row8<UUID, String, String, LocalDateTime, UUID, String, LocalDateTime, LocalDateTime> fieldsRow() {
        return (Row8) super.fieldsRow();
    }
}
