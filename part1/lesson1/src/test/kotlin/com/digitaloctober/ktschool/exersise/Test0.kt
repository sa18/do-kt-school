package com.digitaloctober.ktschool.exersise

import mu.KotlinLogging
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertTrue
import kotlin.reflect.full.companionObject

class Test0 {
    // сначала пробуем без companion
    companion object {
        private val log1 = org.slf4j.LoggerFactory.getLogger(Test0::class.java)
        private val log2 = getMyLogger(this.javaClass)
        private val log3 by lazy { getMyLogger(this.javaClass) }
        private val log = KotlinLogging.logger {}

        @BeforeAll
        @JvmStatic
        fun init() {
            log.info { "Starting All Tests" }
        }

        @AfterAll
        @JvmStatic
        fun afterAll() {
            log.info { "All Tests Complete" }
        }
    }

    @BeforeEach
    fun beforeEachTest(testInfo: TestInfo) {
        log.info { "* начинаем новый тест ${testInfo.displayName} * " }
    }

    @Test
    fun test() {
        log1.info("Привет, мир.")
        log2.info("Привет, мир.")
        log3.info("Привет, мир (lazyly)")
        assertTrue(true)
    }

    @Test
    fun test2() {
        log.info { "Привет, мир! (приемлемый вариант)" }
    }
}

private inline fun <T : Any> getMyLogger(cl: Class<T>) =
    org.slf4j.LoggerFactory.getLogger(unwrapCompanionClass(cl))

// unwrap companion class to enclosing class given a Java Class
fun <T : Any> unwrapCompanionClass(ofClass: Class<T>): Class<*> {
    return if (ofClass.enclosingClass != null &&
        ofClass.enclosingClass.kotlin.companionObject?.java == ofClass
    ) {
        ofClass.enclosingClass
    } else {
        ofClass
    }
}
