package com.digitaloctober.ktschool.exersise

import assertk.assertThat
import assertk.assertions.isEqualTo
import com.digitaloctober.ktschool.personModelV2.Person
import com.digitaloctober.ktschool.personModelV2.PersonMapper
import mu.KotlinLogging
import org.junit.jupiter.api.Test
import org.mapstruct.factory.Mappers
import java.time.LocalDate

class Test2 {

    private val log = KotlinLogging.logger {}

    private val person = Person(
        "Дональд",
        "Трамп",
        "+1 555 000-00-00",
        LocalDate.parse("1946-06-14")
    )

    @Test
    fun `через MapStruct`() {

        val converter = Mappers.getMapper(PersonMapper::class.java)

        val personV2 = converter.toV2(person)
        log.debug { personV2 }

        val personV1Again = converter.toV1(personV2)
        log.debug { personV1Again }

        //assertThat(person.toString()).isEqualTo(personV1Again.toString())
    }
}
