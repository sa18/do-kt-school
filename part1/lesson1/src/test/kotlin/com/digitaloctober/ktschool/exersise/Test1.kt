package com.digitaloctober.ktschool.exersise

import com.digitaloctober.ktschool.personModel.Person
import com.digitaloctober.ktschool.personModel.PersonV2
import mu.KotlinLogging
import org.junit.jupiter.api.Test
import java.time.LocalDate
import kotlin.reflect.KProperty1
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.primaryConstructor

class Test1 {

    private val log = KotlinLogging.logger {}

    private val person = Person(
        "Дональд",
        "Трамп",
        "+1 555 000-00-00",
        LocalDate.parse("1946-06-14")
    )

    @Test
    fun `создание объекта`() {
        log.debug { person }
    }

    @Test
    fun `простое преобразование`() {
        val personV2 = with(person) {
            PersonV2(
                firstName = firstName,
                lastName = lastName,
                phone = phoneNumber,
                birthdate = birthdate
            )
        }
        log.debug { personV2 }
    }

    @Test
    fun `через Reflection`() {
        /*person::class.memberProperties.forEach {
            log.debug { "member ${it.name}" }
        }*/

        val sourceObject = person
        val sourceFieldsMap = sourceObject::class.memberProperties.map { it.name to it }.toMap()

        val args = PersonV2::class.primaryConstructor!!.parameters!!.map { param ->
            val fieldName = when (param.name) {
                PersonV2::phone.name -> Person::phoneNumber.name
                else -> param.name
            }

            fieldName to sourceFieldsMap[fieldName]?.let {
                it as KProperty1<Person, *>
                it.get(sourceObject)
            }
        }

        log.debug { args }

        val personV2 = PersonV2::class.primaryConstructor!!.call(*args.map { it.second }.toTypedArray())
        log.debug { personV2 }
    }

}
