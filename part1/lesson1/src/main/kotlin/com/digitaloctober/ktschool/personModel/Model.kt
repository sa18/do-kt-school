package com.digitaloctober.ktschool.personModel

import java.time.LocalDate

data class Person(val firstName: String, val lastName: String, val phoneNumber: String, val birthdate: LocalDate)

data class PersonV2(val firstName: String, val lastName: String, val phone: String, val birthdate: LocalDate)
