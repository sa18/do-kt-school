package com.digitaloctober.ktschool.personModelV2

import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import java.time.LocalDate


// https://ru.stackoverflow.com/questions/991071/kotlin-modelmapper-defaultconstructormarker

/*
data class Person @JvmOverloads constructor(
    var firstName: String? = null,
    var lastName: String? = null,
    var phoneNumber: String? = null,
    var birthdate: LocalDate? = null
)

data class PersonV2 @JvmOverloads constructor(
    var FIO: String? = null,
    var phone: String? = null,
    var birthdate: LocalDate? = null
)
*/

data class Person(
    var firstName: String? = null,
    var lastName: String? = null,
    var phoneNumber: String? = null,
    var birthdate: LocalDate? = null
) {
    constructor() : this(null, null, null, null)
}

data class PersonV2(
    var FIO: String? = null,
    var phone: String? = null,
    var birthdate: LocalDate? = null
) {
    constructor() : this(null, null, null)
}

@Mapper
interface PersonMapper {

    @Mappings(
        Mapping(target = "phone", source = "phoneNumber"),
        Mapping(target = "FIO", expression = "java(person.getFirstName() + \" \" + person.getLastName())")
    )
    fun toV2(person: Person): PersonV2

    @InheritInverseConfiguration
    @Mappings(
        Mapping(target = "firstName", expression = "java(person.getFIO().split(\" \")[0])"),
        Mapping(target = "lastName", expression = "java(person.getFIO().split(\" \")[1])")
    )
    fun toV1(person: PersonV2): Person
}
