import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    kotlin("kapt")
}

repositories {
    mavenCentral()
    //maven { setUrl("https://jitpack.io") }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.3.50")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.3.50")
    implementation("io.github.microutils:kotlin-logging:1.7+")
    implementation("ch.qos.logback:logback-classic:1.2.+")
    implementation("org.mapstruct:mapstruct:1.3.0.Final")
    kapt("org.mapstruct:mapstruct-processor:1.3.0.Final")
    // not working :(
    //implementation("com.github.pozo.mapstruct-kotlin:mapstruct-kotlin:1.3.0.Beta2")
    //kapt("com.github.pozo.mapstruct-kotlin:mapstruct-kotlin-processor:1.3.0.Beta2")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.+")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.+")
    //testImplementation("org.assertj:assertj-core:3.+")
    testImplementation("com.willowtreeapps.assertk:assertk-jvm:0.20")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
        suppressWarnings = true
        freeCompilerArgs = listOf<String>("-Xjsr305=strict")
    }
}

kapt {
    correctErrorTypes = true
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform { }
}
