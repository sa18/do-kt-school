import React from 'react';
import './App.css';

class State { i: Number = 0 }
class Props { initialI: Number = 0; title: String }
export class Counter extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {i: props.initialI}
    }
    render() {
        return <div><a href="javascript:;"
                       onClick={this.onClick.bind(this)}>{this.props.title}</a>
            {this.state.i}
        </div>
    }
    onClick() {
        this.setState({i: this.state.i + 1})
    }
}
