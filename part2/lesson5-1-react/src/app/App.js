import React from 'react';
import './App.css';
import {Counter} from "./Counter";

export class App extends React.Component<{}, {}> {
	render() {
        return <div>
            <Counter initialI={500} title="Привет React!"/>
        </div>
    }
}
