pluginManagement {
    resolutionStrategy {
        eachPlugin {
            when (requested.id.id) {
                "com.moowork.node" -> useModule("com.moowork.gradle:gradle-node-plugin:1.2.0")
                //"com.eriwen.gradle.js" -> useModule("com.eriwen:gradle-js-plugin:1.12.1")
                //"net.eikehirsch.react" -> useModule("net.eikehirsch.react:gradle-react-plugin:0.4.1")
            }
            if (requested.id.id == "kotlin2js") {
                useModule("org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.21")
            }
        }
    }
}

rootProject.name = "part2"

include(":lesson5-1-react")
include(":lesson5-2-ktreact")
include(":lesson6-obank-web-ui")
