package app

import antd.breadcrumb.breadcrumb
import antd.breadcrumb.breadcrumbItem
import antd.icon.icon
import antd.layout.content
import antd.layout.footer
import antd.layout.layout
import antd.layout.sider
import antd.menu.menu
import antd.menu.menuItem
import antd.menu.subMenu
import api.CustomerProfile
import kotlinext.js.js
import kotlinx.coroutines.MainScope
import kotlinx.css.*
import react.*
import react.dom.header
import react.dom.span
import styled.css
import styled.styledDiv
import styled.styledImg

data class LoginInfo(var customerProfile: CustomerProfile) {
    val isLoggedIn get() = customerProfile != null
}

object AppState {
    // Глобальная (для приложения) область выполнения корутин
    val coroutineScope = MainScope()

    // Контексты
    val loginInfoCtx = createContext<LoginInfo>()
}

class AppView : RComponent<RProps, RState>() {

    init {
        //kotlinext.js.require("src/app/App.css")
        kotlinext.js.require("antd/dist/antd.css")
        //requireAll(require.context("src", true, js("/\\.css$/")))
    }

    override fun RBuilder.render() {
        layout {
            header(classes = "header") {
                styledDiv {
                    css {
                        float = Float.left
                        paddingRight = LinearDimension("1em")
                        paddingLeft = LinearDimension("1em")
                    }
                    styledImg(src = appLogo) {
                        css {
                            width = LinearDimension("50px")
                            height = LinearDimension("50px")
                            marginLeft = LinearDimension("5px")
                            marginTop = LinearDimension("5px")
                        }
                    }
                }
                menu {
                    attrs {
                        theme = "dark"
                        mode = "horizontal"
                        style = js { lineHeight = "64px" }
                    }
                    menuItem {
                        attrs.key = "1"
                        +"Продукты"
                    }
                    menuItem {
                        attrs.key = "2"
                        +"Услуги"
                    }
                    menuItem {
                        attrs.key = "3"
                        +"О компании"
                    }
                }
            }
            layout {
                sider {
                    attrs {
                        width = 200
                        style = js { background = "#fff" }
                    }
                    menu {
                        attrs {
                            mode = "inline"
                            defaultSelectedKeys = arrayOf("1")
                            style = js {
                                height = "100%"
                                borderRight = 0
                            }
                        }
                        subMenu {
                            attrs {
                                key = "sub1"
                                title = buildElement {
                                    span {
                                        icon {
                                            attrs.type = "user"
                                        }
                                        +"пункт меню 1"
                                    }
                                }
                            }
                            menuItem {
                                attrs.key = "1"
                                +"опция1"
                            }
                            menuItem {
                                attrs.key = "2"
                                +"опция2"
                            }
                            menuItem {
                                attrs.key = "3"
                                +"опция3"
                            }
                            menuItem {
                                attrs.key = "4"
                                +"опция4"
                            }
                        }
                        subMenu {
                            attrs {
                                key = "sub2"
                                title = buildElement {
                                    span {
                                        icon {
                                            attrs.type = "laptop"
                                        }
                                        +"пункт меню 2"
                                    }
                                }
                            }
                            menuItem {
                                attrs.key = "5"
                                +"опция5"
                            }
                            menuItem {
                                attrs.key = "6"
                                +"опция6"
                            }
                            menuItem {
                                attrs.key = "7"
                                +"опция7"
                            }
                            menuItem {
                                attrs.key = "8"
                                +"опция8"
                            }
                        }
                        subMenu {
                            attrs {
                                key = "sub3"
                                title = buildElement {
                                    span {
                                        icon {
                                            attrs.type = "notification"
                                        }
                                        +"пункт меню 3"
                                    }
                                }
                            }
                            menuItem {
                                attrs.key = "9"
                                +"опция9"
                            }
                            menuItem {
                                attrs.key = "10"
                                +"опция10"
                            }
                            menuItem {
                                attrs.key = "11"
                                +"опция11"
                            }
                            menuItem {
                                attrs.key = "12"
                                +"опция12"
                            }
                        }
                    }
                }

                layout {
                    attrs.style = js { padding = "0 24px 24px" }
                    breadcrumb {
                        attrs.style = js { margin = "16px 0" }
                        breadcrumbItem { +"Главная" }
                        breadcrumbItem { +"Акции" }
                        breadcrumbItem { +"Акция 1" }
                    }
                    content {
                        attrs.style = js {
                            background = "#fff"
                            padding = "0 24px"
                            minHeight = 280
                            margin = 0
                        }
                        + "(content)"
                    }
                }
            }
            footer {
                attrs.style = js { textAlign = "center" }
                +"(c) нижняя часть "
            }
        }
    }
}

fun RBuilder.app() = child(AppView::class) {}

@JsModule("src/app/bankLogo.jpg")
external val appLogo: dynamic