package api

import binding.AxiosConfigSettings
import binding.axios
import kotlinext.js.jsObject
import kotlinx.coroutines.await

object ServerApi {

    const val serverUrl = "https://a.topso.ru/ies7_obank_api/api/v1"

    data class CustomerRegisterRequest(
            val cardNumber: String,
            val fio: String,
            val mobilePhone: String,
            val email: String
    )

    data class CustomerRegisterResponse(val textualAnswer: String)

    data class CustomerAuthBeginRequest(val mobilePhone: String)
    data class CustomerAuthBeginResponse(val token: String, val textualAnswer: String)

    data class CustomerAuthVerifyRequest(val token: String, val shortCode: String)
    data class CustomerAuthVerifyResponse(
            val numNewChatMessages: Int,
            val numNewPersonalPromoItems: Int,
            val textualAnswer: String
    )

    data class CustomerGetProfileResponse(val id: String, val fio: String, val dtCreated: LocalDateTime, val email: String)

    data class CustomerGetAvatarRequest(val size: String /* small | large */)

    data class CustomerChangeProfileRequest(
            var fio: String? = null,
            var email: String? = null,
            var avatar: ByteArray? = null
    )

    data class AccountGetCardsListResponse(val cardsList: Collection<BankingCard>)

    data class TransactionGetHistoryRequest(
            val accountNumber: String,
            val dateFrom: LocalDate,
            val dateTillInclusive: LocalDate
    )

    data class TransactionGetHistoryResponse(val items: Collection<TransactionHistoryItem>)

    data class TransactionDetailsResponse(val details: TransactionDetails)

    data class ChatGetHistoryRequest(val dateFrom: LocalDate)
    data class ChatGetHistoryResponse(val messagesList: List<ChatMessage>)

    data class ChatPostMessageRequest(val clientSideMessageId: String, val messageText: String)
    data class ChatPostMessageResponse(val clientSideMessageId: String, val serverSideMessageId: String)

    data class PromoGetItemsRequest(val newOnly: Boolean = false, val dateFrom: LocalDate)
    data class PromoGetItemsResponse(val items: List<PromoItem>)

    object CustomerController {
        suspend fun register(request: CustomerRegisterRequest): CustomerRegisterResponse =
                postAndParseResult("$serverUrl/customer/register", request)

        suspend fun authBegin(request: CustomerAuthBeginRequest): CustomerAuthBeginResponse =
                getAndParseResult("$serverUrl/customer/auth/begin", request)

        suspend fun authVerify(request: CustomerAuthVerifyRequest): CustomerAuthVerifyResponse =
                getAndParseResult("$serverUrl/customer/auth/verify", request)

        suspend fun getProfile(): CustomerGetProfileResponse =
                getAndParseResult("$serverUrl/customer/profile", emptyList<String>())

        suspend fun changeProfile(request: CustomerChangeProfileRequest): Unit =
                putAndParseResult("$serverUrl/customer/profile", emptyList<String>())

        fun getAvatarRequestPath() = "$serverUrl/customer/avatar"
    }

    object AccountController {
        suspend fun getCardsList(): AccountGetCardsListResponse =
                getAndParseResult("$serverUrl/account/cardsList", emptyList<String>())
    }

    object ChatController {
        suspend fun getChatHistory(request: ChatGetHistoryRequest): ChatGetHistoryResponse =
                postAndParseResult("$serverUrl/chat/history", request)

        suspend fun postMessage(request: ChatPostMessageRequest): ChatPostMessageResponse =
                postAndParseResult("$serverUrl/chat/message", request)
    }

    object PromoEventController {
        suspend fun getPromoItems(request: PromoGetItemsRequest): PromoGetItemsResponse =
                postAndParseResult("$serverUrl/promo-event/items", request)
    }

    object TransactionController {
        suspend fun getHistory(request: TransactionGetHistoryRequest): TransactionGetHistoryResponse =
                postAndParseResult("$serverUrl/transaction/history", request)

        suspend fun getDetails(transactionId: String): TransactionDetailsResponse =
                postAndParseResult("$serverUrl/transaction/details", transactionId)
    }

    object SimulatorController {
        suspend fun listAllCards(): Array<BankingCard> =
                getAndParseResult("$serverUrl/simulator/listAllCards")
    }
}

private val stringify = js("require('json-stable-stringify')") as (obj: Any) -> String

private suspend fun <T> getAndParseResult(url: String, params: Any? = null): T =
        requestAndParseResult("GET", url, params)

private suspend fun <T> postAndParseResult(url: String, params: Any): T =
        requestAndParseResult("POST", url, params)

private suspend fun <T> putAndParseResult(url: String, params: Any): T =
        requestAndParseResult("PUT", url, params)

private suspend inline fun <T> requestAndParseResult(requestMethod: String, requestUrl: String, requestParams: Any?): T {
    val config: AxiosConfigSettings = jsObject {
        url = requestUrl
        params = requestParams
        method = requestMethod
        withCredentials = true
    }
    val data = axios<T>(config).await().data
    return data
}
