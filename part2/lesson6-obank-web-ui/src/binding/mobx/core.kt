package binding.mobx

import kotlin.browser.window
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

typealias IReactionDisposer = () -> Unit

@JsModule("mobx")
@JsNonModule
external object Mobx {
    @JsName("observable")
    fun observable(value: String): IObservableValue<String>

    @JsName("observable")
    fun observable(value: Int): IObservableValue<Int>

    @JsName("observable")
    fun <T> observable(value: T): IObservableValue<T>

    @JsName("observable")
    fun <A> observable(array: Array<A>): IObservableArray<A>

    @JsName("autorun")
    fun autorun(block: () -> Unit): IReactionDisposer

    @JsName("reaction")
    fun reaction(expression: Any, effect: Any): IReactionDisposer

    @JsName("reaction")
    fun reaction(expression: Any, effect: Any, opts: Any?): IReactionDisposer

    @JsName("extendObservable")
    fun extendObservable(instance: Any, props: dynamic)

    @JsName("action")
    fun action(block: () -> Unit): () -> dynamic

    @JsName("action")
    fun action(actionName: String, block: () -> Unit): () -> dynamic

    @JsName("runInAction")
    fun <T> runInAction(block: () -> T): T

    @JsName("runInAction")
    fun <T> runInAction(actionName: String, block: () -> T): T

    @JsName("decorate")
    fun <R : Any> decorate(obj: R, decorators: dynamic)

    @JsName("observable")
    object Observable {
        @JsName("box")
        fun <T> box(value: T?, options: CreateObservableOptions?): IObservableValue<T>

        //@JsName("map")
        //fun <K, V> map(value: Map<K, V>)
    }

    @JsName("computed")
    fun computed(a1: Any?, a2: Any?)

    @JsName("computed")
    fun computed(a1: Any?, a2: Any?, a3: Any?)

}

inline fun <reified A> observable(list: List<A>): IObservableArray<A> = Mobx.observable(list.toTypedArray())

fun kaction(block: () -> Unit) = window.setTimeout(Mobx.action(block), 0)
fun kaction(actionName: String, block: () -> Unit) = window.setTimeout(Mobx.action(actionName, block), 0)

class CreateObservableOptions(
    val name: String? = null,
    val deep: Boolean? = null,
    val proxy: Boolean? = null
    // defaultDecorator
    // equals
)

@JsName("IObservableValue")
external interface IObservableValue<T> {
    fun get(): T
    fun set(newValue: T)
}

class ObservableProperty<T>(initialValue: T? = null, options: CreateObservableOptions? = CreateObservableOptions(deep = true)) : ReadWriteProperty<Any?, T> {
    private val boxedValue = Mobx.Observable.box(initialValue, options)
    override fun getValue(thisRef: Any?, property: KProperty<*>): T = boxedValue.get() ?: throw NullPointerException()
    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        boxedValue.set(value)
    }
}

@JsName("IObservableArray")
external class IObservableArray<A> {
    // Javascript array methods

    fun push(item: A)
    val length: Int
    fun <B> map(xform: (A) -> B): Array<B>
    fun forEach(process: (A) -> Unit)

    // Mobx's array extensions

    fun clear(): Array<A>
    fun remove(value: A): Boolean
    fun toJS(): Array<A>
    fun toJSON(): Array<A>
}
