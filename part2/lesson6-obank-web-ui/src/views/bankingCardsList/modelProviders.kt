package views.bankingCardsList

import api.BankingCard
import api.ServerApi
import app.AppState
import kotlinx.coroutines.launch

class ServerModelProvider {
    val model = Model()
    fun load() = AppState.coroutineScope.launch {
        model.dataReady = false
        model.list.clear()
        //console.log("Запрос к серверу")
        val result = ServerApi.SimulatorController.listAllCards()
        //console.log("получен ответ с данными $result")
        model.list += result
        model.dataReady = true
    }
}


class TestModelProvider {
    val model = Model()

    init {
        with(model) {
            dataError = false
            dataReady = true
            dataLoading = false
            list.add(BankingCard("123456", "123456", "2020-06-06T18:00:00", null))
            list.add(BankingCard("567890", "567890", "2020-06-06T18:00:00", null))
        }
    }
}
