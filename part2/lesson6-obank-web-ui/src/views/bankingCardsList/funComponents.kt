package views.bankingCardsList

import api.BankingCard
import api.hasOwnerCustomer
import api.maskedCardNumber
import api.paysysName
import kotlinx.css.CSSBuilder
import kotlinx.css.LinearDimension
import kotlinx.css.height
import kotlinx.css.width
import kotlinx.html.title
import moment.moment
import react.RProps
import react.child
import react.dom.small
import react.dom.span
import react.dom.td
import react.dom.tr
import react.functionalComponent
import styled.css
import styled.styledImg

fun displayBankingCard(card: BankingCard) = functionalComponent<RProps> {
    tr(classes = "caption") {
        td {
            attrs.colSpan = "2"
            child(displayPaysysLogo(card.paysysName))
            +" "
            +card.maskedCardNumber
        }
    }
    tr {
        td { +"Номер счёта" }
        td { +card.accountNumber }
    }
    tr {
        td { +"Действительна до" }
        td { +moment(card.dateValidTill).format("DD.MM.YYYY") }
    }
    if (card.hasOwnerCustomer) {
        tr {
            td {
                attrs.colSpan = "2"
                +"Привязана к пользователю "
                span {
                    attrs.title = card.ownerCustomerId!!
                    small {
                        +"(id)"
                    }
                }
            }
        }
    }
}

fun displayPaysysLogo(psName: String) = functionalComponent<RProps> {
    val style: CSSBuilder.() -> Unit = {
        width = LinearDimension("40px")
        height = LinearDimension("25px")
    }

    when (psName) {
        "VISA" -> styledImg(src = logoVisa, alt = "visa") { css(style) }
        else -> styledImg(src = logoMastercard, alt = "mc") { css(style) }
    }
}

@JsModule("src/views/bankingCardsList/mastercard.jpg")
external val logoMastercard: dynamic
@JsModule("src/views/bankingCardsList/visa.jpg")
external val logoVisa: dynamic
