package views.bankingCardsList

import binding.mobx.MobxReact
import binding.mobx.observer
import react.*
import react.dom.*

class BankingCardsListView(props: RProps) : RComponent<RProps, RState>(props) {
    private val modelProvider = ServerModelProvider()
    //private val modelProvider = TestModelProvider()
    private val model get() = modelProvider.model

    override fun componentDidMount() {
        modelProvider.load()
    }

    override fun RBuilder.render() {
        with(model) {

            if (isDataLoading) {
                +"Загрузка..."
            }

            if (isDataError) {
                +"Ошибка загрузки данных :("
                return
            }

            if (!isDataReady) {
                return
            }

            if (list.isEmpty()) {
                +"(empty)"
                return
            }

            table(classes = "BankingCardsList") {
                thead {
                    tr {
                        td {
                            attrs.colSpan = "2"
                            +"Всего карточек (${list.size})"
                        }
                    }
                }
                tbody {
                    list.forEach {
                        child(displayBankingCard(it))
                    }
                }
            }
        }
    }

    companion object {
        init {
            MobxReact.observer(BankingCardsListView::class)
        }
    }
}

fun RBuilder.bankingCardsListView(handler: RHandler<RProps>) = child(BankingCardsListView::class, handler)
