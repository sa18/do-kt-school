package views.bankingCardsList

import api.BankingCard
import binding.mobx.ObservableProperty

class Model {
    val list by ObservableProperty(mutableListOf<BankingCard>())
    var dataReady by ObservableProperty(false)
    var dataError by ObservableProperty(false)
    var dataLoading by ObservableProperty(false)
    val isDataReady get() = dataReady
    val isDataError get() = dataError
    val isDataLoading get() = dataLoading
}