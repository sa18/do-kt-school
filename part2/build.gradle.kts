group = "org.ktmsk"
version = "0.1.0-DEV"

plugins {
    id("kotlin2js") version "1.3.50" apply false
}

repositories {
    jcenter()
}

subprojects {
    apply("${project.rootDir}/versions.gradle.kts")
}
