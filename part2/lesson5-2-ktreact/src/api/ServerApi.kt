package api

import kotlinext.js.jsObject
import kotlinx.coroutines.await

object ServerApi {

    suspend fun listAllCards(): Array<BankingCard> {

        val config: AxiosConfigSettings = jsObject {
            method = "GET"
            url = "https://a.topso.ru/ies7_obank_api/api/v1/simulator/listAllCards"
            withCredentials = true
        }

        return axios<Array<BankingCard>>(config).await().data
    }
}
