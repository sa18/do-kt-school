package api

import kotlin.math.max

val BankingCard.paysysName: String
    get() =
        cardNumber[0].toInt().let {
            when {
                it % 2 == 0 -> "VISA"
                else -> "MasterCard"
            }
        }

val BankingCard.maskedCardNumber: String
    get() {
        val n = max(1, cardNumber.length - 4)
        return "…" + cardNumber.substring(n, cardNumber.length)
    }

val BankingCard.hasOwnerCustomer
    get() =
        ownerCustomerId != null
