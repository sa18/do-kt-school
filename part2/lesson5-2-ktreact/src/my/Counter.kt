package my

import react.*
import kotlin.browser.window
import kotlin.properties.Delegates

class Counter(props: Props) : RComponent<Counter.Props, Counter.State>(props) {

    class Props(var title: String, var initialValue: Int) : RProps
    class State(var value: Int) : RState

    private var timerID by Delegates.notNull<Int>()

    init {
        state = State(props.initialValue)
    }

    override fun componentDidMount() {
        timerID = window.setInterval(::onTimer, 1000)
    }

    override fun componentWillUnmount() {
        window.clearInterval(timerID)
    }

    override fun RBuilder.render() {
        +"${props.title}: ${state.value}"
    }

    private fun onTimer() {
        setState { value = state.value + 1 }
    }
}

// Для поддержки DSL
fun RBuilder.counter(initer: Counter.Props.() -> Unit) =
    child(Counter::class) { attrs.apply(initer) }

fun RBuilder.counter(title: String, initialValue: Int = 0) = child(Counter::class) {
    attrs.apply {
        this.title = title
        this.initialValue = initialValue
    }
}
