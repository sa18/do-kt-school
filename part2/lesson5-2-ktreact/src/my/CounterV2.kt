package my

import binding.mobx.MobxReact
import binding.mobx.ObservableProperty
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import kotlin.browser.window
import kotlin.properties.Delegates

private class CounterStore(initialValue: Int) {

    var value by ObservableProperty<Int>(initialValue)
    private var timerID by Delegates.notNull<Int>()

    init {
        timerID = window.setInterval(::tick, 1000)
    }

    fun tick() {
        ++value
    }

    fun dispose() {
        window.clearInterval(timerID)
    }
}

class CounterV2(props: Props) : RComponent<CounterV2.Props, RState>(props) {

    class Props(var title: String, var initialValue: Int) : RProps

    private val store = CounterStore(props.initialValue)

    override fun componentWillUnmount() {
        store.dispose()
    }

    override fun RBuilder.render() {
        val msg = "${props.title}: ${store.value}"
        +msg
    }
}

private val counterObserver = MobxReact.observer(CounterV2::class.js)

fun RBuilder.counterV2(title: String, initialValue: Int = 0) = child(CounterV2::class) {
    attrs.title = title
    attrs.initialValue = initialValue
}
