package my

import api.*
import app.AppState
import kotlinx.coroutines.launch
import kotlinx.css.CSSBuilder
import kotlinx.css.LinearDimension
import kotlinx.css.height
import kotlinx.css.width
import kotlinx.html.title
import binding.mobx.MobxReact
import binding.mobx.ObservableProperty
import binding.mobx.observer
import moment.moment
import react.*
import react.dom.*
import styled.css
import styled.styledImg

fun RBuilder.bankingCardsListView(handler: RHandler<RProps>) = child(BankingCardsList.View::class, handler)

object BankingCardsList {

    class Model {
        val list by ObservableProperty(mutableListOf<BankingCard>())
        var dataReady by ObservableProperty(false)
        var dataError by ObservableProperty(false)
        var dataLoading by ObservableProperty(false)
        val isDataReady get() = dataReady
        val isDataError get() = dataError
        val isDataLoading get() = dataLoading
    }

    class View(props: RProps) : RComponent<RProps, RState>(props) {
        private val modelProvider = ServerModelProvider()
        //private val modelProvider = TestModelProvider()
        private val model get() = modelProvider.model

        override fun componentDidMount() {
            modelProvider.load()
        }

        override fun RBuilder.render() {
            with(model) {

                if (isDataLoading) {
                    +"Загрузка..."
                }

                if (isDataError) {
                    +"Ошибка загрузки данных :("
                    return
                }

                if (!isDataReady) {
                    return
                }

                if (list.isEmpty()) {
                    +"(empty)"
                    return
                }

                table(classes = "BankingCardsList") {
                    thead {
                        tr {
                            td {
                                attrs.colSpan = "2"
                                +"Всего карточек (${list.size})"
                            }
                        }
                    }
                    tbody {
                        list.forEach {
                            child(displayBankingCard(it))
                        }
                    }
                }
            }
        }

        companion object {
            init {
                MobxReact.observer(View::class)
            }
        }
    }

    class ServerModelProvider {
        val model = Model()
        fun load() = AppState.coroutineScope.launch {
            model.dataReady = false
            model.list.clear()
            //console.log("Запрос к серверу")
            val result = ServerApi.listAllCards()
            //console.log("получен ответ с данными $result")
            model.list += result
            model.dataReady = true
        }
    }

    class TestModelProvider {
        val model = Model()

        init {
            with(model) {
                dataError = false
                dataReady = true
                dataLoading = false
                list.add(BankingCard("123456", "123456", "2020-06-06T18:00:00", null))
                list.add(BankingCard("567890", "567890", "2020-06-06T18:00:00", null))
            }
        }
    }

    fun displayBankingCard(card: BankingCard) = functionalComponent<RProps> {
        tr(classes = "caption") {
            td {
                attrs.colSpan = "2"
                child(displayPaysysLogo(card.paysysName))
                +" "
                +card.maskedCardNumber
            }
        }
        tr {
            td { +"Номер счёта" }
            td { +card.accountNumber }
        }
        tr {
            td { +"Действительна до" }
            td { +moment(card.dateValidTill).format("DD.MM.YYYY") }
        }
        if (card.hasOwnerCustomer) {
            tr {
                td {
                    attrs.colSpan = "2"
                    +"Привязана к пользователю "
                    span {
                        attrs.title = card.ownerCustomerId!!
                        small {
                            +"(id)"
                        }
                    }
                }
            }
        }
    }

    fun displayPaysysLogo(psName: String) = functionalComponent<RProps> {
        val style: CSSBuilder.() -> Unit = {
            width = LinearDimension("40px")
            height = LinearDimension("25px")
        }

        when (psName) {
            "VISA" -> styledImg(src = logoVisa, alt = "visa") { css(style) }
            else -> styledImg(src = logoMastercard, alt = "mc") { css(style) }
        }
    }
}

@JsModule("src/my/mastercard.jpg")
external val logoMastercard: dynamic
@JsModule("src/my/visa.jpg")
external val logoVisa: dynamic
