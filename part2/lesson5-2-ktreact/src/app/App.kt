package app

import kotlinx.coroutines.MainScope
import kotlinx.css.Color
import my.BankingCardsList
import my.bankingCardsListView
import my.counter
import my.counterV2
import react.*
import react.dom.div

object AppState {
    // Глобальная (для приложения) область выполнения корутин
    val coroutineScope = MainScope()

    // Контексты
    val colorCtx = createContext<Color>()
}

class AppView : RComponent<RProps, RState>() {
    override fun RBuilder.render() {
        div {
            counter(title = "Счётчик React State", initialValue = 500)
        }
        div {
            counterV2(title = "Счётчик через Auto Observable State", initialValue = 500)
        }
        div {
            bankingCardsListView { }
        }
    }
}

fun RBuilder.app() = child(AppView::class) {}