package binding.mobx

import react.*
import kotlin.reflect.KClass

@JsModule("mobx-react")
@JsNonModule
external object MobxReact {

    @JsName("observer")
    fun <P : RProps, T : RClass<P>> observer(targetClass: T): T

    @JsName("observer")
    fun <P : RProps, S : RState, T : Component<P, S>> observer(targetComponent: T): T

    @JsName("observer")
    fun <P : RProps, T : FunctionalComponent<P>> observer(targetComponent: T): T

    @JsName("observer")
    fun observer(js: JsClass<*>): Any

    @JsName("IWrappedComponent")
    class IWrappedComponent<P : RProps, S : RState> {
        val wrappedComponent: Component<P, S>
    }

    @JsName("inject")
    fun <P : RProps, S : RState, C : Component<P, S>> inject(vararg storeNames: String): (comp: C) -> IWrappedComponent<P, S>

    @JsName("Provider")
            /*class Provider<P : RProps> : Component<P, RState> {
                override fun render(): dynamic = definedExternally
            }*/
    fun <S : RConsumerProps<*>> Provider(vararg stores: S): dynamic

}

inline fun <P : RProps, S : RState, T : Component<P, S>> MobxReact.observer(comp: T) =
    observer(targetComponent = comp)
inline fun <P : RProps, S : RState, T : RComponent<P, S>> MobxReact.observer(cl: KClass<T>) =
    observer(targetClass = cl.rClass)
