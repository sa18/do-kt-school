import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet
import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile

plugins {
    id("kotlin2js")
}

val kotlinVersion: String by project.extra
val kotlinReactVersion: String by project.extra
val kotlinReactDomVersion: String by project.extra
val kotlinMomentVersion: String by project.extra
val kotlinStyledVersion: String by project.extra

repositories {
    jcenter()
    maven("https://dl.bintray.com/kotlin/kotlin-js-wrappers")
    maven("https://dl.bintray.com/kotlinspain/kotlin-js-wrappers")
}

dependencies {
    implementation(kotlin("stdlib-js"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.+")
    implementation("org.jetbrains:kotlin-react:$kotlinReactVersion-kotlin-$kotlinVersion")
    implementation("org.jetbrains:kotlin-react-dom:$kotlinReactDomVersion-kotlin-$kotlinVersion")
    implementation("org.jetbrains:kotlin-styled:$kotlinStyledVersion-kotlin-$kotlinVersion")
    implementation("io.kotlinspain:kotlin-moment:$kotlinMomentVersion-kotlin-$kotlinVersion")
}

sourceSets["main"].withConvention(KotlinSourceSet::class) {
    kotlin.srcDirs("src")
}

tasks.withType<Kotlin2JsCompile> {
    kotlinOptions {
        outputFile = "$projectDir/build/classes/main/${project.name}.js"
        moduleKind = "commonjs"
        sourceMap = true
        sourceMapEmbedSources = "always"
    }
}
