group = "org.ktmsk"
version = "0.1.0-DEV"

plugins {
    kotlin("jvm").version("1.3.61") apply false
}

repositories {
    jcenter()
}

subprojects {
    apply("${project.rootDir}/versions.gradle.kts")
}
