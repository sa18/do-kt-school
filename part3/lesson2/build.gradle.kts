import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    id("org.jetbrains.kotlin.plugin.jpa").version("1.3.61") // генерирует всем @Entity пустой конструктор
    id("org.jetbrains.kotlin.plugin.spring").version("1.3.61") // делает компоненты Spring открытыми
}

val kotlinVersion: String by project.extra
val junitVersion: String by project.extra
val kotlinLoggingVersion: String by project.extra
val slf4jVersion: String by project.extra
val logbackVersion: String by project.extra
val h2Version: String by project.extra
val springBootVersion: String by project.extra

repositories {
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("io.github.microutils:kotlin-logging:$kotlinLoggingVersion")
    implementation("org.slf4j:slf4j-api:$slf4jVersion")
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("com.h2database:h2:$h2Version")
    testImplementation("org.junit.jupiter:junit-jupiter:$junitVersion")

    // Spring Boot
    implementation("org.springframework.boot:spring-boot-starter:$springBootVersion")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:$springBootVersion")
    //implementation("org.springframework.boot:spring-boot-starter-web:$springBootVersion")
    testImplementation("org.springframework.boot:spring-boot-starter-test:$springBootVersion") {
        exclude(module = "junit")
    }
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.+")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        suppressWarnings = true
        jvmTarget = "1.8"
    }
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}
//
//allOpen {
//    annotation("javax.persistence.Entity")
//    annotation("javax.persistence.MappedSuperclass")
//    annotation("javax.persistence.Embeddable")
//    // + компоненты Spring добавляются автоматически через plugin kotlin-spring
//}
