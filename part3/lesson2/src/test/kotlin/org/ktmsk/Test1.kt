package org.ktmsk

import mu.KotlinLogging
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ContextConfiguration
import org.springframework.transaction.annotation.Transactional

@SpringBootTest
@ContextConfiguration(classes = [_TestsConfig::class])
@Transactional
//@Rollback(false)
class Test1 {

    private val log = KotlinLogging.logger { }

    @Test
    fun test1() {
        log.debug("OK")
    }

}