package org.ktmsk

import org.ktmsk.models.ZooJpaEntityModel
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EnableAutoConfiguration
@EnableConfigurationProperties(DataSourceProperties::class)
//@ComponentScan(basePackageClasses = [ZooRepository::class])
@EnableTransactionManagement
@EnableJpaRepositories
@EntityScan(basePackageClasses = [ZooJpaEntityModel::class])
class _TestsConfig
