package org.ktmsk.models

import javax.persistence.*

typealias ID = Int

object ZooJpaEntityModel {

    @Entity(name = "ZOO")
    data class Zoo(
        @Id
        @GeneratedValue
        val id: ID = 0,
        @Column(nullable = false)
        val title: String
    ) {
        @OneToMany(mappedBy = "zoo", fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
        val aviaries: MutableSet<Aviary> = HashSet()
    }

    @Entity(name = "AVIARY")
    data class Aviary(
        @Id
        @GeneratedValue
        val id: ID = 0,
        @Column(nullable = false)
        val title: String,
        @Column(nullable = false)
        val location: String
    ) {
        @OneToMany(mappedBy = "aviary", fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
        val animals: MutableSet<Animal> = HashSet()

        @ManyToOne(optional = false)
        lateinit var zoo: Zoo

        val square get() = animals.size * 10
    }

    @Entity(name = "ANIMAL")
    data class Animal(
        @Id
        @GeneratedValue
        val id: ID = 0,
        @Column(nullable = false)
        val kind: String,
        @Column(nullable = false)
        val nickname: String,
        @Column(nullable = false)
        val color: String
    ) {
        @ManyToOne(optional = false)
        lateinit var aviary: Aviary
        @ManyToOne(optional = false)
        lateinit var homeZoo: Zoo
    }

    val fixture1: Zoo
        get() {

            val tigerAviary = Aviary(
                title = "Вольер с тиграми",
                location = "0.00"
            ).apply {
                animals.add(Animal(kind = "тигр", nickname = "Петя", color = "orange"))
                animals.add(Animal(kind = "тигр", nickname = "Вася", color = "white"))
                animals.forEach { it.aviary = this }
            }

            val goatsAviary = Aviary(
                title = "Вольер с козлами",
                location = "0.00"
            ).apply {
                animals.add(Animal(kind = "козёл", nickname = "Тимур", color = "silver"))
                animals.forEach { it.aviary = this }
            }

            return Zoo(
                title = "Московский зоопарк"
            ).apply {
                aviaries.add(tigerAviary)
                aviaries.add(goatsAviary)
                aviaries.forEach {
                    it.zoo = this
                    it.animals.forEach { it.homeZoo = this }
                }
            }
        }

}