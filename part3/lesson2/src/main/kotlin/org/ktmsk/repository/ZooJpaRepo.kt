package org.ktmsk.repository

import org.ktmsk.models.ID
import org.ktmsk.models.ZooJpaEntityModel.Animal
import org.ktmsk.models.ZooJpaEntityModel.Aviary
import org.ktmsk.models.ZooJpaEntityModel.Zoo
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ZooRepository : CrudRepository<Zoo, ID>

@Repository
interface AviaryRepository : CrudRepository<Aviary, ID>

@Repository
interface AnimalRepository : CrudRepository<Animal, ID> {
    fun findAllByKind(kind: String): MutableIterable<Animal>
}
