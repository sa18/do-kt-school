pluginManagement {
    resolutionStrategy {
        eachPlugin {
            when (requested.id.id) {
            }
        }
    }
}

rootProject.name = "part3"

include(":lesson1")
include(":lesson2")
include(":lesson3:db")
include(":lesson3:logic")
