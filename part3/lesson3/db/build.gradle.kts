import org.jooq.codegen.GenerationTool
import org.jooq.meta.jaxb.*
import org.jooq.meta.jaxb.Configuration
import org.jooq.meta.jaxb.Target

buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath("org.jooq:jooq:3.+")
        classpath("org.jooq:jooq-meta:3.+")
        classpath("org.jooq:jooq-codegen:3.+")
        classpath("com.h2database:h2:1.+")
    }
}

val h2Version: String by project.extra
val jooqVersion: String by project.extra
val flywayVersion: String by project.extra

plugins {
    `java-library`
    id("org.flywaydb.flyway") version "5.2.4"
}

repositories {
    mavenCentral()
}

dependencies {
    api("javax.annotation:javax.annotation-api:1.+")
    //implementation("javax.annotation:javax.annotation-api:1.3.2") // для JDK8
    implementation("org.jooq:jooq:$jooqVersion")
    implementation("org.jooq:jooq-meta:$jooqVersion")
    implementation("com.h2database:h2:$h2Version")
    testImplementation("org.jooq:jooq-codegen:$jooqVersion")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}


flyway {
    url = "jdbc:h2:file:~/data/lesson3;AUTO_SERVER=true"
    user = "sa"
    password = ""
    schemas = arrayOf("PUBLIC")
    locations = arrayOf("filesystem:src/main/resources/db/migration/")
}

tasks.register("jooqCodegen") {
    doLast {
        val configuration = Configuration().apply {
            jdbc = Jdbc().apply {
                url = flyway.url
                user = "sa"
                password = ""
                driver = "org.h2.Driver"
            }
            generator = Generator().apply {
                database = Database().apply {
                    name = "org.jooq.meta.h2.H2Database"
                    includes = ".*"
                    inputSchema = "PUBLIC"
                    isOutputSchemaToDefault = true
                }
                generate = Generate().apply {
                    withJavaTimeTypes(true)
                }
                target = Target().apply {
                    packageName = "org.ktmsk.db.schema_public"
                    directory = "src/main/java"
                }
            }
        }
        GenerationTool.generate(configuration)
    }
}
