package org.ktmsk

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EnableAutoConfiguration
@EnableConfigurationProperties(DataSourceProperties::class)
//@ComponentScan(basePackageClasses = [ZooRepository::class])
@EnableTransactionManagement
class _TestsConfig
