package org.ktmsk

import mu.KotlinLogging
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.jupiter.api.Test
import org.ktmsk.models.ZooExposedEntityModel
import org.ktmsk.models.ZooExposedTablesModel
import org.ktmsk.models.ZooRelationalModel

class Test1 {

    private val log = KotlinLogging.logger {}

    @Test
    fun `проект удачно сконфигурирован и запускается`() {
        log.debug("Привет")
    }

    @Test
    fun testExposed() {
        Database.connect("jdbc:h2:file:~/data/lesson1;AUTO_SERVER=true", driver = "org.h2.Driver")
        transaction {
            log.info("Создаём таблицы БД")
            addLogger(StdOutSqlLogger)
            SchemaUtils.create(
                ZooExposedTablesModel.ANIMAL,
                ZooExposedTablesModel.AVIARY,
                ZooExposedTablesModel.ZOO
            )
            commit()
            log.info("таблицы созданы")

            log.info("записываем данные")

            ZooExposedEntityModel.Zoo.new(1) {
                title = "Московский зоопарк"
            }

            with(ZooExposedEntityModel.Aviary) {
                new(1) {
                    title = "Вольер с тиграми"
                    zooId = 1
                    location = "123"
                }
                new(2) {
                    title = "Вольер с козлами"
                    zooId = 1
                    location = "234"
                }
            }

            with(ZooExposedEntityModel.Animal) {
                new(1) {
                    kind = "тигр"
                    nickname = "Петя"
                    color = "orange"
                    aviaryId = 1
                }
                new(2) {
                    kind = "тигр"
                    nickname = "Вася"
                    color = "black"
                    aviaryId = 1
                }
                new(3) {
                    kind = "козёл"
                    nickname = "Тимур"
                    color = "silver"
                    aviaryId = 2
                }
            }
        }
    }

}
