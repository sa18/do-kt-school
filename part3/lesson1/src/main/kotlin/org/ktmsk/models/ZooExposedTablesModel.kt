package org.ktmsk.models

import org.jetbrains.exposed.dao.IntIdTable

object ZooExposedTablesModel {

    object ZOO : IntIdTable() {
        //val id = integer("id").autoIncrement().primaryKey()
        val title = varchar("title", 50)
    }

    object AVIARY : IntIdTable() {
        val zooId = integer("zoo_id").references(ZOO.id)
        val title = varchar("title", 50)
        val location = varchar("location", 10)
    }

    object ANIMAL : IntIdTable() {
        val aviaryId = integer("aviary_id").references(AVIARY.id)
        val kind = varchar("kind", 50)
        val nickname = varchar("nick_name", 255)
        val color = varchar("color", 20)
    }

}
