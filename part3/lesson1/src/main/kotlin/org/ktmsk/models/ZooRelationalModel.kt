package org.ktmsk.models

typealias ID = Int

object ZooRelationalModel {

    data class Zoo(val id: ID, val title: String)
    data class Aviary(val id: ID, val zooId: ID, val title: String, val location: String)
    data class Animal(val id: ID, val aviaryId: ID, val kind: String, val nickname: String, val color: String)

    data class DataSet(
        val zoos: Collection<Zoo>,
        val aviaries: Collection<Aviary>,
        val animals: Collection<Animal>
    )

    val fixture1
        get() = DataSet(
            zoos = setOf(Zoo(1, "Московский зоопарк")),
            aviaries = setOf(
                Aviary(1, 1, "Вольер с тиграми", "0.00"),
                Aviary(2, 1, "Вольер с козлами", "0.00")
            ),
            animals = setOf(
                Animal(1, 1, "тигр", "Петя", "orange"),
                Animal(2, 1, "тигр", "Вася", "black"),
                Animal(3, 2, "козёл", "Тимур", "silver")
            )
        )
}
