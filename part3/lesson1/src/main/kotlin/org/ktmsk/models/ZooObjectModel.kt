package org.ktmsk.models

object ZooObjectModel {

    data class Zoo(val title: String, val aviaries: Collection<Aviary>)

    data class Aviary(val name: String, val location: String, val animals: Collection<Animal>) {
        val square get() = animals.size * 10
    }

    data class Animal(val kind: String, val nickname: String, val color: String) {
        lateinit var homeZoo: Zoo
    }

}
