package org.ktmsk.models

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.ktmsk.models.ZooExposedTablesModel.ANIMAL
import org.ktmsk.models.ZooExposedTablesModel.AVIARY
import org.ktmsk.models.ZooExposedTablesModel.ZOO

object ZooExposedEntityModel {

    class Zoo(id: EntityID<Int>) : IntEntity(id) {
        companion object : IntEntityClass<Zoo>(ZOO)
        var title by ZOO.title
    }

    class Aviary(id: EntityID<Int>) : IntEntity(id) {
        companion object : IntEntityClass<Aviary>(AVIARY)
        var zooId by AVIARY.zooId
        var title by AVIARY.title
        var location by AVIARY.location
    }

    class Animal(id: EntityID<Int>) : IntEntity(id) {
        companion object : IntEntityClass<Animal>(ANIMAL)
        var aviaryId by ANIMAL.aviaryId
        var kind by ANIMAL.kind
        var nickname by ANIMAL.nickname
        var color by ANIMAL.color
    }

}
